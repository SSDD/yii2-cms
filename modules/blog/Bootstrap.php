<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 20.03.2015
 * Time: 11:04
 */

namespace modules\blog;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface {

    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app) {

        $frontPostRule = [
            'pattern' => '<alias:[0-9a-zA-Z\-]+>',
            'route' => 'blog/post/index',
            'suffix' => '.html'
        ];

        $frontPostWithCategoryRule = \Yii::$app->getModule('blog')->getParam('categoryInUrlRule');
//            [
//                'pattern' => '<category:[0-9a-zA-Z\-]+>/<id:\d+>-<alias:[0-9a-zA-Z\-]+>',
//                'route' => 'blog/post/with-category',
//                'suffix' => '.html'
//            ];


        $frontPostWithCategoryPagerRule = \Yii::$app->getModule('blog')->getParam('categoryInUrlPagerRule');
//            [
//                'pattern' => '<category:[0-9a-zA-Z\-]+>/<id:\d+>-<alias:[0-9a-zA-Z\-]+>/<pageNum:[0-9]+>',
//                'route' => 'blog/post/with-category',
//                'suffix' => '.html'
//            ];


        $frontCategoryRule = [
            'pattern' => '<alias:[\w\-]+>',
            'route' => 'blog/category/index',
            'suffix' => '/'
        ];

        // Route rules for backend
        if (isset(\Yii::$app->modules['blog']->isBackend) && \Yii::$app->modules['blog']->isBackend) {

            $app->getUrlManager()->addRules([
                // BlogController
                '' => 'blog/blog/index',
                //'login' => 'blog/blog/login',
                '<_a:(login|logout|error)>' => 'blog/blog/<_a>',
                'clear-cache/<type:[a-z\-]+>' => 'blog/blog/clear-cache',
                // CategoryController
                [
                    'pattern' => 'categories',
                    'route' => 'blog/category/index'
                ],
                [
                    'pattern' => 'category/create',
                    'route' => 'blog/category/create'
                ],
                'category/<_a:(update|delete|sort)>/<id:\d+>' => 'blog/category/<_a>',
                // PostController
                'posts' => 'blog/post/index',
                'post/create' => 'blog/post/create',
                'post/<_a:(update|delete)>/<id:\d+>' => 'blog/post/<_a>',
                'post/crop-me/<id:\d+>' => 'blog/post/crop-me',
                'post/images-get/<id:\d+>' => 'blog/post/images-get',
                'post/image-remove/<id:\d+>' => 'blog/post/image-remove',
                // for select2 autocomplete widget
                'post/post-list' => 'blog/post/post-list',
                'user/user-list' => 'blog/user/user-list',
                // CommentController
                'comments' => 'blog/comment/index',
                'comment/create' => 'blog/comment/create',
                'comment/<_a:(view|update|delete)>/<id:\d+>' => 'blog/comment/<_a>',
                '<_m:\w+>/<_c:\w+>/<_a:\w+>' => '<_m>/<_c>/<_a>',
            ], false);

            $app->setComponents([
                'urlManagerFrontend' => [
                    'class' => 'yii\web\UrlManager',
                    'baseUrl' => '/',
                    'enablePrettyUrl' => true,
                    'showScriptName' => false,
                    'rules' => [
                        // Main page
                        '' => 'blog/blog/index',
                        // Post page
                        $frontPostRule,
                        // Post with category in url
                        $frontPostWithCategoryRule,
                        // Category page
                        $frontCategoryRule,
                    ]
                ]
            ]);

        }
        // Route rules for frontend
        else {
            $app->getUrlManager()->addRules([
                // BlogController
                '' => 'blog/blog/index',
                '<_a:(about|signup|login|logout|search-bar|search|captcha|error|sitemap|footer|privacy)>' => 'blog/blog/<_a>',
                [
                    'pattern' => 'contact',
                    'route' => 'blog/blog/contact',
                    //'suffix' => '.html'
                ],
                'request-password-reset' => 'blog/blog/request-password-reset',
                // Category with pager
                [
                    'pattern' => '<alias:[\w\-]+>/<pageNum:\d+>',
                    'route' => 'blog/category/index',
                    'suffix' => '/'
                ],
                // Category page
                $frontCategoryRule,
                // Post Sidebar
                'post-bar' => 'blog/post/post-bar',
                // Social Share
                'social' => 'blog/post/social-share',
                // Post page with pager
                [
                    'pattern' => '<alias:[0-9a-zA-Z\-]+>/<pageNum:[0-9]+>',
                    'route' => 'blog/post/index',
                    'suffix' => '.html',
                ],
                // Post page
                $frontPostRule,
                // Post page with category in url and with pager
                $frontPostWithCategoryPagerRule,
                // Post with category in url
                $frontPostWithCategoryRule,
                'add-comment' => 'blog/comment/create',
                //'<_m:\w+>/<_c:\w+>/<_a:\w+>' => '<_m>/<_c>/<_a>',
            ], false);

            $app->setComponents([
                'urlManagerBackend' => [
                    'class' => 'yii\web\UrlManager',
                    'baseUrl' => '/backend',
                    'enablePrettyUrl' => true,
                    'showScriptName' => false,
                    'rules' => [
                        '' => 'blog/blog/index'
                    ]
                ]
            ]);
        }

        $messagesBasePath = $app->getModule('blog')->getParam('messagesBasePath');

        // Add module I18N category.
        if (!isset($app->i18n->translations['modules/blog*']) && !isset($app->i18n->translations['modules/*'])) {
            $app->i18n->translations['modules/blog*'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => $messagesBasePath,
                //'forceTranslation' => true,
                'fileMap' => [
                    'modules/blog' => 'blog.php',
                    'modules/blog/category' => 'category.php',
                    'modules/blog/post' => 'post.php',
                    'modules/blog/comment' => 'comment.php',
                    'modules/blog/user' => 'user.php',
                ]
            ];
        }

    }
}