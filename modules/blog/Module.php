<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 18.03.2015
 * Time: 16:13
 */

namespace modules\blog;
use Yii;

class Module extends \modules\blog\components\Module {

    public $isBackend = false;

    /**
     * Constants for turning on/off or hiding comments on the blog
     */
    const BLOG_COMMENTS_ON = 1;
    const BLOG_COMMENTS_OFF = 2;
    const BLOG_COMMENTS_HIDDEN = 3;

    public static $name = 'blog';

    public $defaultParams = [
        'blogName' => 'Заголовок главной страницы сайта',
        'blogTitle' => 'Тайтл главной страницы сайта',
        'blogDescription' => '',
        'blogKeywords' => '',
        'feed' => 'http://feeds.feedburner.com/your-feed-name',
        'twitterAccount' => 'your-twitter-account',
        /**
         * @var int Cache duration in seconds: 86400 (24 hours = 3600*24)
         */
        'cacheDuration' => 86400,
        'imagesBaseUrl' => '/images/',
        /**
         * @var int Posts per page
         */
        'postsPerPage' => 10,
        /**
         * @var int Comments per page
         */
        'commentsPerPage' => 10,
        /**
         * @var int Posts per sidebar
         */
        'postsPerSidebar' => 7,
        /**
         * @var int Comments per sidebar
         */
        'commentsPerSidebar' => 5,
        /**
         * @var bool True if Sphinx is installed on server
         */
        'isSphinxEnabled' => false,
        /**
         * @var string The name of Sphinx fulltext index.
         */
        'sphinxIndex' => 'is_index',
        /**
         * @var string The name of Sphinx runtime index.
         */
        'sphinxRtIndex' => 'is_rt',
        /**
         * @var int limits the Sphinx search results
         */
        'sphinxSearchLimit' => 20,
        /**
         * @var int The number of posts in the "Related Posts" block.
         */
        'relatedPostsLimit' => 3,
        /**
         * This option is used for tuning post url-structure.
         * 1) `false` value:
         *    urls like this: is.cms/hello-world-post.html
         *
         *    `true` value:
         *    urls like this: http://is.cms/life-hack/12-how-i-spant-summer-holidays.html
         *
         *  2) After turning this option on (`true`) you will see additional field `Category in url`
         *     in post edit form in backend. Select the category you want to see in the certain post-url.
         *     Now you can move this post to any category you like, but the url will still contain the category
         *     from `Category in url` field.
         *
         * @var bool
         */
        'categoryInUrl' => false,
        'categoryInUrlRule' => [
            'pattern' => '<category:[0-9a-zA-Z\-]+>/<id:\d+>-<alias:[0-9a-zA-Z\-]+>',
            'route' => 'blog/post/with-category',
            'suffix' => '.html'
        ],
        'categoryInUrlPagerRule' => [
            'pattern' => '<category:[0-9a-zA-Z\-]+>/<id:\d+>-<alias:[0-9a-zA-Z\-]+>/<pageNum:[0-9]+>',
            'route' => 'blog/post/with-category',
            'suffix' => '.html'
        ],
        /**
         * @var int Image quality for saving
         */
        'imageQuality' => 90,
        /**
         * @var int Post image width.
         * Used for cropping and resizing in in \modules\blog\models\Post->afterSave().
         */
        'imgPostWidth' => 300,
        'imgPostHeight' => 200,
        /**
         * @var int Thumb image width.
         * More details in $imagePostWidth property.
         */
        'imgThumbWidth' => 160,
        'imgThumbHeight' => 120,
        /**
         * @var int Mini image width.
         * Mini images are shown mostly in side bar blocks.
         * More details in $imagePostWidth property.
         */
        'imgMiniWidth' => 70,
        'imgMiniHeight' => 63,
        /**
         * @var int Full image width.
         * Mini images are shown mostly in side bar blocks.
         * More details in $imagePostWidth property.
         */
        'imgFullWidth' => 1500,
        'imgFullHeight' => 1000,
        /**
         * @var int
         */
        'blogCommentsStatus' => self::BLOG_COMMENTS_ON,
        'messagesBasePath' => '@modules/blog/messages',
    ];

    public function getParam($name) {
        return isset($this->params[$name]) ? $this->params[$name] :
                        (isset($this->defaultParams[$name]) ? $this->defaultParams[$name] :
                            'invalid blog module param: '.$name);
    }

    public function init() {
        parent::init();
    }

    public static function powered() {
        return 'Powered by <a href="http://yiiframework.com" rel="external">yiiframework</a>';
    }

}