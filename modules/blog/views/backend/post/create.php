<?php

use yii\helpers\Html;
use modules\blog\Module;

/* @var $this yii\web\View */
/* @var $model modules\blog\models\Post */

$this->title = Module::t('blog/post', 'New Post');
$this->params['breadcrumbs'][] = ['label' => Module::t('blog/post', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'user_id' => $user_id,
        'user_name' => $user_name,
        'categoriesArray' => $categoriesArray,
        'categoriesOptions' => $categoriesOptions,
        'statusArray' => $statusArray,
        'commentsStateArray' => $commentsStateArray,
    ]) ?>

</div>
