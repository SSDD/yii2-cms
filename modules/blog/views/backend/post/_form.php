<?php

//use Yii;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use modules\blog\Module;
use kartik\datetime\DateTimePicker;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model modules\blog\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
    if (!$model->isNewRecord) {
        echo Html::tag('span', '', ['id' => 'imperCropMePlugin', 'value' => Yii::$app->urlManager->createUrl(['blog/post/crop-me', 'id' => $model->id])]);
    }
?>

<div class="post-form">
    <?php
        $form = ActiveForm::begin([
            'id' => 'postForm',
            'options' => ['enctype'=>'multipart/form-data'],
        ]);
    ?>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <?= $form->field($model, 'alias')->textInput(['maxlength' => 255]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4 col-sm-offset-2">
            <?php
            echo $form->field($model, 'category_id')->dropDownList(
                $categoriesArray,
                [
                    'options' => $categoriesOptions,
                    'prompt' => Module::t('blog/post', 'Select Category')
                ])->label(Module::t('blog/category', 'Category'));
            ?>
        </div>

        <div class="col-sm-2">
            <?= $form->field(Yii::$app->user->identity, 'name')
                ->textInput(['value' => $user_name, 'name' => 'user_name', 'disabled' => 'disabled'])
                ->label(Module::t('blog/post', 'Author Name'));
            ?>
            <?= $form->field($model, 'user_id')->hiddenInput(['value' => $user_id])->label(false) ?>
        </div>
    </div>

    <?php if (Yii::$app->getModule('blog')->getParam('categoryInUrl')): ?>
    <div class="row">
        <div class="col-sm-4 col-sm-offset-2">
            <?php
            echo $form->field($model, 'category_url_id')->dropDownList(
                $categoriesArray,
                [
                    'options' => $categoriesOptions,
                    'prompt' => Module::t('blog/post', 'Select Category')
                ])->label(Module::t('blog/category', 'Category in url'));
            ?>
        </div>
    </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-4 col-sm-offset-2">
            <?= $form->field($model, 'tags')->textInput(['rows' => 6]) ?>
        </div>

        <div class="col-sm-3">
            <?= $form->field($model, 'date')->widget(DateTimePicker::classname(), [
                    //'language' => 'ru',
                    'type' =>  DateTimePicker::TYPE_COMPONENT_APPEND,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd hh:ii:ss',
                        'default' => date('y-m-d H:i:s'),
                        'todayHighlight' => true
                    ],
                ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <?= $form->field($model, 'keywords')->textInput(['maxlength' => 255]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <?= $form->field($model, 'description')->textArea(['rows' => 2]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <?php
                $plugins = ['scriptbuttons' => 'modules\blog\widgets\cropme\ImperScriptBtnsAsset'];
                if (!$model->isNewRecord) {
                    $plugins['impercropme'] = 'modules\blog\widgets\cropme\ImperCropMeAsset';
                }

                echo $form->field($model, 'anons')->widget(Widget::className(), [
                    'settings' => [
                        'lang' => Yii::$app->language,
                        'placeholder' => 'Anons text...',
                        'minHeight' => 60,
                        // Settings for ImperCropMe img manager
                        'uploadAndCrop' => false,
                        'imageManagerJson' => Yii::$app->urlManager->createUrl(['blog/post/images-get', 'id' => $model->id]).'?path=thumb',
                        'imageResizable' => false,
                        'imageEditable' => false,
                        'replaceDivs' => false,
                        'paragraphize' => false,
                        'convertImageLinks' => false,
                        'imageLink' => false,
                    ],
                    'plugins' => $plugins
                ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <?php
                echo $form->field($model, 'text')->widget(Widget::className(), [
                    'options' => ['id' => 'post-txt'],
                    'settings' => [
                        'placeholder' => 'Post text...',
                        'lang' => Yii::$app->language,
                        'minHeight' => 200,
                        'formatting' => ['h2', 'h3', 'p', 'blockquote', 'pre', 'div'],
                        // Settings for ImperCropMe img manager
                        'uploadAndCrop' => true,
                        'imageManagerJson' => Yii::$app->urlManager->createUrl(['blog/post/images-get', 'id' => $model->id]),
                        'imageRemoveJson' => Yii::$app->urlManager->createUrl(['blog/post/image-remove', 'id' => $model->id]),
                        'imageResizable' => false,
                        'imageEditable' => false,
                        'replaceDivs' => false,
                        'paragraphize' => false,
                        'convertImageLinks' => false,
                        'imageLink' => false,
                        'plugins' => [
                            'fullscreen',
                            'video',
                            'table',
                            'textexpander',
                            'fontcolor',
                        ]
                    ],
                    'plugins' => $plugins
                ]);

            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-2 col-sm-offset-3">
            <?= $form->field($model, 'comments_state')->dropDownList($commentsStateArray)->label(false); ?>
        </div>

        <div class="col-sm-2">
            <?= $form->field($model, 'status')->dropDownList($statusArray)->label(false); ?>
        </div>

        <div class="col-sm-4">
            <?= Html::submitButton($model->isNewRecord ? Module::t('blog', 'Create') : Module::t('blog', 'Save'), ['class' => ($model->isNewRecord ? 'btn btn-success' : 'btn btn-primary')]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
