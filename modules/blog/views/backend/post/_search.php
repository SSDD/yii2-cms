<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use modules\blog\Module;

/* @var $this yii\web\View */
/* @var $model modules\blog\models\backend\PostSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'category_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'keywords') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'alias') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'anons') ?>

    <?php // echo $form->field($model, 'text') ?>

    <?php // echo $form->field($model, 'images') ?>

    <?php // echo $form->field($model, 'tags') ?>

    <?php // echo $form->field($model, 'comments_count') ?>

    <?php // echo $form->field($model, 'published') ?>

    <?php // echo $form->field($model, 'visits') ?>

    <div class="form-group">
        <?= Html::submitButton(Module::t('blog', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Module::t('blog', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
