<?php

use yii\helpers\Html;
use yii\grid\GridView;
use modules\blog\Module;
use modules\blog\components\ContentHelper;

/* @var $this yii\web\View */
/* @var $searchModel modules\blog\models\backend\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('blog/post', 'Posts');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Module::t('blog/post', 'Create Post'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                //'class' => DataColumn::className(),
                'attribute' => 'id',
                'contentOptions'=>['style'=>'width: 3%;']
            ],
            'date',
            [
                'attribute' => 'name',
                'format' =>'raw',
                'value' => function($model) {
                    $img = '';
                    if ($model->images) {
                        $img = Html::img(
                            ContentHelper::getImageSrc('mini', $model->images),
                            ['style' => 'float:left;margin-right:8px;border-radius:4px;']
                        );
                    }

                    return $img . Html::a(
                        $model->name,
                        ContentHelper::getPostUrl($model->category_url_id, $model->id, $model->alias, null, true),
                        ['target' => '_blank']
                    );
                }
            ],
            [
                'attribute' => 'categoryName',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'category_id',
                    $categoriesArray,
                    [
                        'class' => 'form-control',
                        'prompt' => Module::t('blog/post', 'Select Category'),
                        'encode' => false,
                        'options' => $categoriesOptions
                    ]
                ),

            ],
            [
                'attribute' => 'user_id',
                'value' => 'userName',
                'label' => Module::t('blog/post', 'Author Name')
            ],
            [
                'attribute' => 'statusName',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    $statusArray,
                    [
                        'class' => 'form-control',
                        'prompt' => Module::t('blog/post', 'Select Status')
                    ]
                )
            ],
            [
                'attribute' => 'commentsStateName',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'comments_state',
                    $commentsStateArray,
                    [
                        'class' => 'form-control text',
                        'prompt' => Module::t('blog/post', 'Select State')
                    ]
                )
            ],
            // 'visits',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>

</div>
