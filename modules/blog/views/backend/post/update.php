<?php

use yii\helpers\HtmlPurifier;
use modules\blog\Module;

/* @var $this yii\web\View */
/* @var $model modules\blog\models\Post */

$this->title = Module::t('blog/post', 'Update {modelClass}: ', [
    'modelClass' => 'Post',
]) . ' ' . HtmlPurifier::process($model->name);
$this->params['breadcrumbs'][] = ['label' => Module::t('blog/post', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Module::t('blog', 'Update');
?>
<div class="post-update">

    <h1><?= HtmlPurifier::process($model->name); ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'user_id' => $user_id,
        'user_name' => $user_name,
        'categoriesArray' => $categoriesArray,
        'categoriesOptions' => $categoriesOptions,
        'statusArray' => $statusArray,
        'commentsStateArray' => $commentsStateArray,
    ]) ?>

</div>
