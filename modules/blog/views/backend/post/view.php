<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\DetailView;
use modules\blog\Module;

/* @var $this yii\web\View */
/* @var $model modules\blog\models\Post */

$model->name = HtmlPurifier::process($model->name);
$model->title = HtmlPurifier::process($model->title);
$model->anons = HtmlPurifier::process($model->anons);
$model->text = HtmlPurifier::process($model->text);

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Module::t('blog/post', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = HtmlPurifier::process($model->name);
?>
<div class="post-view">

    <h1><?= $model->name ?></h1>

    <p>
        <?= Html::a(Module::t('blog', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Module::t('blog', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Module::t('blog/post', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            //'name',
            'categoryName',
            'statusName',
            'user_id',
            'anons:html',
            'text:html',
            //'date',
            //'title',
            //'keywords',
            //'alias',
            //'category_id',
            //'description',
            //'images:ntext',
            //'tags:ntext',
            //'comments_count',
            //'status:ntext',
            //'visits',
        ],
    ]) ?>

</div>
