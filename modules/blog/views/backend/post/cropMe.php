<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 21.04.2015
 * Time: 13:56
 */
use yii\widgets\ActiveForm;
use modules\blog\widgets\cropme\CropMe;

?>

<div class="post-form">

    <?php
        $form = ActiveForm::begin([
            'id' => 'cropMeForm',
            'options' => ['enctype'=>'multipart/form-data'],
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
        ]);

        $js =<<<JSCRIPT

            var files;

            // Add events
            $('input[type=file]').on('change', prepareUpload);

            // Grab the files and set them to our variable
            function prepareUpload(event) {
                files = event.target.files;
            }

            $('.btnUpload').click(function() {

                var action = $('#cropMeForm').attr('action');
                var data = new FormData();

                data.append($('#post-alias').attr('name'), $('#post-alias').attr('value'));
                data.append($('#post-crop_info').attr('name'), $('#post-crop_info').attr('value'));

                $.each(files, function(key, value) {
                    data.append($('#post-image').attr('name'), value);
                });

                $('#message').html('uploading...');
                $(document.body).append($('<div id="redactor-progress"><span></span></div>'));
                $('#redactor-progress').fadeIn();

                $.ajax({
                    url: action,
                    type: "POST",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function(response) {
                        console.log('oKay!');
                        //alert('ok!');
                        $('#message').html('done!');

                        $('#redactor-progress').fadeOut(1500, function() {
                            $(this).remove();
                        });
                    },
                    error: function(response) {
                        console.log('erRor..');
                        $('#message').html('error!');
                    }
                });

                return false;
            });

JSCRIPT;

        $this->registerJs($js);

    ?>

    <div class="row">
        <div class="col-sm-12">
        <?= $form->field($model, 'image')->widget(CropMe::className(), [
            'id' => 'cropMeWidget',
            'attributeCropInfo' => 'crop_info',
            'optionsCropbox' => [
                'boxWidth' => 800,
                'boxHeight' => 600,
                'cropSettings' => [
                    [
                        'width' => 350,
                        'height' => 250,

                        'minWidth' => 250,//Min width of thumbBox. By default not used.
                        'maxWidth' => 800,//Max width of thumbBox. By default not used.

                        'minHeight' => 250,//Min height of thumbBox. By default not used.
                        'maxHeight' => 600,//Max height of thumbBox. By default not used.
                    ],
                ],
//                    'messages' => [
//                        'Thumbnail image',
//                        'Small image',
//                    ],
            ],
        ]);


        ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>