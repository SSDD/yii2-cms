<?php

use yii\helpers\Html;
use yii\grid\GridView;
use modules\blog\Module;
use modules\blog\components\ContentHelper;

/* @var $this yii\web\View */
/* @var $searchModel modules\blog\models\backend\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('blog/comment', 'Comments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Module::t('blog/comment', 'Create Comment'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'contentOptions'=>['style'=>'width: 3%;'],
            ],
            [
                'attribute' => 'comment_id',
                'contentOptions'=>['style'=>'width: 3%;'],
            ],
            'date',
            [
                'attribute' => 'post_id',
                'label' => Module::t('blog/post', 'Name'),
                'format' =>'raw',
                'value' => function($model) {
                    return Html::a(
                        $model->postName,
                        Yii::$app->urlManagerFrontend->createUrl(['/blog/post/index', 'alias' => $model->postAlias]),
                        ['target' => '_blank']
                    );
                }
            ],
            [
                'attribute' => 'comment',
                'format' => 'html',
                'value' => function($model) {
                    return ContentHelper::anons($model->comment, 0, 100, true);
                }
            ],
            'user_name',
            [
                'attribute' => 'status',
                'format' => 'html',
                //'format' => 'raw',
                'value' => function ($model) {
                    return $model->statusName;
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    $statusArray,
                    [
                        'class' => 'form-control',
                        'prompt' => Module::t('blog/comment', 'Select Status')
                    ]
                )
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
