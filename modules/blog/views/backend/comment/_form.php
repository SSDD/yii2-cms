<?php

use modules\blog\Module;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;

?>

<div class="comment-form">

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <div class="row">
        <div class="col-sm-3 col-sm-offset-2">
            <?= $form->field($model, 'date')->widget(DateTimePicker::classname(), [
                //'language' => 'ru',
                'type' =>  DateTimePicker::TYPE_COMPONENT_APPEND,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy.mm.dd hh:ii:ss',
                    'default' => date('y.m.d H:i:s'),
                    'todayHighlight' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-sm-5">
            <?= $form->field($model, 'post_id')->widget(Select2::classname(), [
                'id' => 'select2-user-id',
                'language' => 'ru',
                'disabled' => !$model->isNewRecord,
                'initValueText' => $postName, // set the initial display text
                'options' => ['placeholder' => 'Search for a post ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => $postUrl, // 'post/post-list' action
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function (post) { return post.text; }'),
                    'templateSelection' => new JsExpression('function (post) { return post.text; }'),
                ],
            ])->label(Module::t('blog/post', 'Name'));
            ?>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-2 col-sm-offset-2">
            <?php

                echo Html::label(Module::t('blog/comment', 'User Role'), 'user-type');
                $model->user_role = $model->user_id ? 2 : 1;
                echo $form
                        ->field($model, 'user_role')
                        ->label(false)
                        ->radioList(
                            [
                                1 => 'Guest',
                                2 => 'User',
                            ],
                            [
                                'id' => 'user-role',
                                'class' => 'btn-group',
                                'data-toggle' => 'buttons',
                                //'disabled' => 'disabled',
                                'unselect' => null, // remove hidden field
                                'item' => function ($index, $label, $name, $checked, $value) {
                                    return '<label class="user-role btn btn-default' . ($checked ? ' active' : '') . '">' .
                                    Html::radio($name, $checked, ['value' => $value]) . $label . '</label>';
                                },
                            ]);

            ?>
        </div>

        <div class="col-sm-3">
            <span id="user-select">
                <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
                    'language' => 'ru',
                    //'id' => 'xxx',
                    //'disabled' => !$model->isNewRecord,
                    'initValueText' => $userName, // set the initial display text
                    'options' => ['placeholder' => 'Search for a user ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'ajax' => [
                            'url' => $userUrl, // 'user/user-list' action
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression(
                            'function (user) {'.
                            '    if ((user.email)) {'.
                            //'        $("#comment-email").val(user.email);'.
                            '    }'.
                            '    return user.text;'.
                            '}'),
                        'templateSelection' => new JsExpression(
                            'function (user) {'.
                            '    if ((user.email)) {'.
                            '        $("#comment-email").val(user.email);'.
                            '    }'.
                            '    return user.text;'.
                            '}'
                        ),
                    ],
                    'pluginEvents' => [
                        "select2:select" => "function(obj) { console.log('select'); }",
                    ]
                ])->label(Module::t('blog/comment', 'User Name'));
                ?>
            </span>

            <span id="user-guest">
                <?= $form->field($model, 'user_name',[
                        'inputOptions' => [
                            'class' => 'form-control',
                            'placeholder' => Module::t('blog/comment', 'Input user name...')
                        ]
                    ])->textInput() ?>
            </span>

        </div>

        <div class="col-sm-3">
            <?= $form->field($model, 'email')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-2 col-sm-offset-4">
            <?= $form->field($model, 'status')->dropDownList($statusArray)->label(false); ?>
        </div>

        <div class="col-sm-4">
            <?= Html::submitButton($model->isNewRecord ? Module::t('blog', 'Create') : Module::t('blog', 'Save'), ['class' => ($model->isNewRecord ? 'btn btn-success' : 'btn btn-primary')]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$js = <<< JS

    var user_role = 1; // is guest
    $('.user-role').each(function(i){
        if ($(this).hasClass('active')) {
            user_role = $(this).find('input').attr('value');
        }
    });

    ShowHideCommentUser(user_role, false);

    $('.user-role').change(function(){
        ShowHideCommentUser($(this).find('input').attr('value'), true);
    });

    function ShowHideCommentUser(user_role, clear) {
        var hide_field_id = 'user-select';
        var show_field_id = 'user-guest';
        if (user_role == 2) {
            hide_field_id = 'user-guest';
            show_field_id = 'user-select';
            if (clear) {
                $("#comment-user_name").val('');
                $("#comment-email").val('');
            }
        }
        else {
            if (clear) {
                $('#comment-user_id').select2('val', null);
                $("#comment-user_name").val('');
                $("#comment-email").val('');
            }
        }

        $('#'+hide_field_id).hide();
        $('#'+show_field_id).show();
    }
JS;

$this->registerJs($js);
