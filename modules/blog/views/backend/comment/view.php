<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use modules\blog\Module;

/* @var $this yii\web\View */
/* @var $model modules\blog\models\Comment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Module::t('blog/comment', 'Comments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'post_id',
            'postName',
            'comment_id',
            'user_id',
            'user_name',
            'email',
            'comment:ntext',
            'date',
            'status',
            'ip',
        ],
    ]) ?>

</div>
