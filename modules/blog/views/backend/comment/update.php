<?php

use yii\helpers\Html;
use modules\blog\Module;

/* @var $this yii\web\View */
/* @var $model modules\blog\models\Comment */

$this->title = Module::t('blog/comment', 'Update Comment');//'Update Comment: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Module::t('blog/comment', 'Comments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Module::t('blog', 'Update');
?>
<div class="comment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'statusArray' => $statusArray,
        'postUrl' => $postUrl,
        'postName' => $postName,
        'userUrl' => $userUrl,
        'userName' => $userName,
    ]) ?>

</div>
