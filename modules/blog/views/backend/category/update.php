<?php

use modules\blog\Module;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/* @var $this yii\web\View */
/* @var $model modules\blog\models\Category */

$this->title = Module::t('blog/category', 'Update {modelClass}: ', [
    'modelClass' => 'Category',
]) . ' ' . HtmlPurifier::process($model->title);
$this->params['breadcrumbs'][] = ['label' => Module::t('blog/category', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Module::t('blog', 'Update');
?>
<div class="category-update">

    <h1><?= HtmlPurifier::process($model->name) ?></h1>

    <?= $this->render('_form', [
        'categoriesArray' => $categoriesArray,
        'categoriesOptions' => $categoriesOptions,
        'model' => $model,
    ]) ?>

</div>
