<?php

use modules\blog\Module;
use yii\helpers\Html;
use himiklab\sortablegrid\SortableGridView as GridView;

/* @var $this yii\web\View */
/* @var $searchModel modules\blog\models\backend\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('blog/category', 'Categories');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Module::t('blog/category', 'Create Category'), ['category/create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'name',
                'format' =>'raw',
                'label' => Module::t('blog/category', 'Category Name'),
                'content' => function($model) use ($categories) {
                    $parentsArray = [];
                    $parents = '';
                    $parent_id = $model->parent_id;

                    while (true) {
                        if (!$parent_id) {
                            break;
                        }

                        $parentsArray[] =
                            '<button class="btn btn-xs btn-default" title="'.$categories[$parent_id]['name'].'">'.
                                '<span class="glyphicon glyphicon-arrow-right"></span>'.
                            '</button> ';
                        $parent_id = $categories[$parent_id]['parent_id'];
                    }

                    if (count($parentsArray)) {
                        krsort($parentsArray);
                        $parents = implode(' ', $parentsArray);
                    }

                    return $parents . Html::a(
                        $model->name,
                        Yii::$app->urlManagerFrontend->createUrl(['/blog/category/index', 'alias' => $model->alias]),
                        ['target' => '_blank']
                    );
                }
            ],
            'alias',
            [
                'attribute' => 'postCount',
                'contentOptions'=>['style'=>'width: 6%;text-align:center;']
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions'=>['style'=>'width: 5%;'],
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>

</div>
