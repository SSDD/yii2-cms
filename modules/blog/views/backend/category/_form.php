<?php

use modules\blog\Module;
use modules\blog\models\Category;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model modules\blog\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-sm-4 col-sm-offset-2">
            <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'alias')->textInput(['maxlength' => 255]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4 col-sm-offset-2">
            <?= $form->field($model, 'parent_id')->dropDownList(
                $categoriesArray,
                [
                    'options' => $categoriesOptions,
                    'prompt' => Module::t('blog/category', 'Select Category'),
                ]);
            ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'crumb')->textInput(['maxlength' => 255]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <?= $form->field($model, 'imageFile')->fileInput(); ?>
            <?php if ($model->image): ?>
                <?= Html::img(Yii::$app->getModule('blog')->getParam('imagesBaseUrl').'category/'.$model->image, ['width'=>'100px']); ?>
                <br>
                <?= $form->field($model, 'imageRemove')->checkBox(); ?>
            <?php endif; ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <?= $form->field($model, 'snippet')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="form-group text-center">
        <?= Html::submitButton($model->isNewRecord ? Module::t('blog', 'Create') : Module::t('blog', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
