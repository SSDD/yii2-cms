<?php

use modules\blog\Module;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model modules\blog\models\Category */

$this->title = Module::t('blog/category', 'New Category');
$this->params['breadcrumbs'][] = ['label' => Module::t('blog/category', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'categoriesArray' => $categoriesArray,
        'categoriesOptions' => $categoriesOptions,
        'model' => $model,
    ]) ?>

</div>
