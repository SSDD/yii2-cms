<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use modules\blog\Module;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \models\frontend\PasswordResetRequestForm */

$this->title = Module::t('blog', 'Request password reset');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset">
    <div class="row">
        <div class="col-sm-5 col-sm-offset-4">
            <h1><?= $this->title ?></h1>

            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                <?= $form->field($model, 'email', ['inputOptions' => ['placeholder' => Module::t('blog', 'Your e-mail')]])->label(false); ?>
                <div class="form-group">
                    <?= Html::submitButton(Module::t('blog', 'Send'), ['class' => 'btn btn-primary']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
