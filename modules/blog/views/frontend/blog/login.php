<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use modules\blog\Module;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = Module::t('blog', 'Authentication');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4">
            <h1><?= Html::encode($this->title) ?></h1>

            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <?= $form->field($model, 'username', ['inputOptions' => ['placeholder' => Module::t('blog', 'Username')]])->label(false); ?>
                <?= $form->field($model, 'password', ['inputOptions' => ['placeholder' => Module::t('blog', 'Password')]])->label(false)->passwordInput() ?>
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
                <div style="color:#999;margin:1em 0">
                    <?php
                        $resetUrl = Html::a(Module::t('blog', 'reset it'), ['blog/request-password-reset']);
                        echo Module::t('blog', 'If you forgot your password you can {reset it}.', ['reset it' => $resetUrl]);
                    ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton(Module::t('blog', 'Login'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
