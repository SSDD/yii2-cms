<?php
use yii\helpers\Html;
use modules\blog\Module;

/* @var $this yii\web\View */
$this->title = Module::t('blog', 'Privacy Policy');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-privacy">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>You may modify the following file to customize its content:</p>
</div>
