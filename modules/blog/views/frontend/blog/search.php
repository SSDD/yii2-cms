<?php

use yii\helpers\Html;
use modules\blog\Module;

$this->title = Module::t('blog', 'Your search results ({count})', ['count' => count($searchModels)]);
$this->params['breadcrumbs'][] = Module::t('blog', 'Site Search');
?>

<div class="row">
    <aside class="col-sm-4 col-sm-push-8">
        <?= $this->render('/blocks/category_bar') ?>
    </aside>

    <div class="col-sm-8 col-sm-pull-4">
        <div class="post-item">
            <h1><span><?= $this->title; ?></span></h1>
            <p>
                <?= Module::t('blog', 'Searched phrase: &laquo;<b>{searchPhrase}</b>&raquo;', ['searchPhrase' =>$searchPhrase]); ?>
            </p>
        </div>

        <?php
            foreach ($searchModels as $model) {
                echo $this->render('/blocks/_search_post', [
                    'model' => $model,
                    'snippet' => $snippets[$model->id],
                    'category' => $categories[$model->category_id],
                ]);
            }
        ?>
    </div>
</div>
