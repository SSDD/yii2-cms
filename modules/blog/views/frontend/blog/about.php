<?php
use yii\helpers\Html;
use modules\blog\Module;

/* @var $this yii\web\View */
$this->title = Module::t('blog', 'About');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>This is the About page. You may modify the following file to customize its content:</p>
</div>
