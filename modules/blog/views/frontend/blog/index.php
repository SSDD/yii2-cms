<?php
/* @var $this yii\web\View */

use modules\blog\Module;
use modules\blog\widgets\sections\Sections;
//@modules/blog/widgets/sections/views

$this->title = $this->context->module->getParam('blogTitle');

// meta description

if ($this->context->module->getParam('blogDescription')) {
    $this->registerMetaTag(['description' => $this->context->module->getParam('blogDescription')]);
}

// meta keywords
if ($this->context->module->getParam('blogKeywords')) {
    $this->registerMetaTag(['keywords' => $this->context->module->getParam('blogKeywords')]);
}

?>

<!--<h1>--><?//= $this->context->module->blogName ?><!--</h1>-->
<h1><?= $this->context->module->getParam('blogName') ?></h1>

<div class="row">
    <aside class="col-sm-4 col-sm-push-8">
        <?= $this->render('/blocks/blog_bar') ?>
    </aside>

    <div class="col-sm-8 col-sm-pull-4">
        <?= Sections::widget(
                [
                    'newPostsTitle' => Module::t('blog', 'New Posts'),
                    'newPostsCount' => 5,
                    //'exceptPosts' => [14,16],       // post Ids
                    //'onlyCategories' => [],        // cat. Ids.
                    //'exceptCategories' => [9,11],  // cat. Ids.
                    //'altCategoryNames' => [9=>'про Пол в Доме', 11=>'Тестовый раздел'],  // [$id1=>$name1, $id2=>$name2...]
                ]
            );
        ?>
    </div>
</div>
