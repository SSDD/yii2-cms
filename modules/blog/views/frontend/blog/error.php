<?php

use yii\helpers\Html;
use modules\blog\Module;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<div class="site-error">

    <div class="row">
        <div class="col-sm-offset-1 col-sm-10">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-offset-1 col-sm-10">
        <?= \yii\bootstrap\Alert::widget([
            'body' =>  Module::t('blog', nl2br(Html::encode($message))),
            'options' => ['class' => 'alert-danger text-center'],
        ]); ?>
        </div>
    </div>

    <div class="panel panel-default col-sm-offset-1 col-sm-10">
        <div class="panel-body">
            <p>
                <?= Module::t('blog', 'The above error occurred while the Web server was processing your request.'); ?>
            </p>
            <p>
                <?= Module::t('blog', 'Please contact us if you think this is a server error. Thank you.'); ?>
            </p>
        </div>
    </div>

</div>
