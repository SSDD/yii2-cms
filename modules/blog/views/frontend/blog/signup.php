<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use modules\blog\Module;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model modules\blog\models\frontend\SignupForm */

$this->title = Module::t('blog', 'Signup');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4">
            <h1><?= Html::encode($this->title) ?></h1>

            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                <?= $form->field($model, 'username', ['inputOptions' => ['placeholder' => Module::t('blog', 'Username')]])->label(false); ?>
                <?= $form->field($model, 'fullname', ['inputOptions' => ['placeholder' => Module::t('blog', 'Full Name')]])->label(false); ?>
                <?= $form->field($model, 'email', ['inputOptions' => ['placeholder' => Module::t('blog', 'Your e-mail')]])->label(false); ?>
                <?= $form->field($model, 'password', ['inputOptions' => ['placeholder' => Module::t('blog', 'Password')]])->label(false)->passwordInput() ?>
                <div class="form-group">
                    <?= Html::submitButton(Module::t('blog', 'Signup'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
