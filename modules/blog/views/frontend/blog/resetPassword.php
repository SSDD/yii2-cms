<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use modules\blog\Module;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

$this->title = Module::t('blog', 'Reset password');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset-password">
    <div class="row">
        <div class="col-sm-5 col-sm-offset-4">
            <h1><?= Html::encode($this->title) ?></h1>

            <p><?= Module::t('blog', 'Please choose your new password:'); ?></p>

            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <div class="form-group">
                    <?= Html::submitButton(Module::t('blog', 'Save'), ['class' => 'btn btn-primary']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
