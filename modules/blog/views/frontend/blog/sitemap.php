<?php
use yii\helpers\Html;
use modules\blog\Module;
use modules\blog\components\ContentHelper;

$pageName = Module::t('blog', 'Site Map');
$this->title = $pageName;
$this->params['breadcrumbs'][] = $pageName;

?>

<div class="row">
    <aside class="col-sm-4 col-sm-push-8">
        <?= $this->render('/blocks/category_bar') ?>
    </aside>

    <div class="col-sm-8 col-sm-pull-4">
        <div class="sitemap">
            <h1><?= $pageName; ?></h1>

            <ul class="sitemap-category">
<?php

$i = 0;
$parent_id = 0;

foreach ($categories as $cat) {
    $cat['parent_id'] = intval($cat['parent_id']);

    if ($cat['parent_id']) {
        if ($categories[$cat['parent_id']]['catsCount'] && $cat['parent_id'] !== $parent_id) {
            echo '<ul>'."\n";
        }
    }

    echo '<li>'.Html::a($cat['name'], ['category/index', 'alias' => $cat['alias']]).':'."\n";

    $ul = false;

    while ($i < count($posts)) {
        if (intval($cat['id']) !== intval($posts[$i]['category_id'])) {
            if ($ul) {
                echo '</ul>'."\n";
                $ul = false;
            }
            break;
        }

        if (!$ul) {
            echo '<ul class="sitemap-post">'."\n";
            $ul = true;
        }

        echo '<li>'.Html::a($posts[$i]['name'], ContentHelper::getPostUrl($posts[$i]['category_url_id'], $posts[$i]['id'],$posts[$i]['alias'])/*['post/index', 'alias' => $posts[$i]['alias']]*/).'</li>'."\n";
        $i++;
    }

    if ($ul) {
        echo '</ul>'."\n";
    }

    echo '</li>'."\n";

    if ($cat['parent_id'] && $categories[$cat['parent_id']]['catsCount']) {
        $categories[$cat['parent_id']]['catsCount']--;

        if ( ! $categories[$cat['parent_id']]['catsCount']) {
            echo '</ul>';
        }

        $parent_id = $cat['parent_id'];
    }

}
?>
            </ul>
        </div>
    </div>
</div>