<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use modules\blog\Module;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model modules\blog\models\frontend\ContactForm */

$this->title = Module::t('blog', 'Contact Page');
$this->params['breadcrumbs'][] = Module::t('blog', 'Contact Us');
?>
<div class="site-contact">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <h1><?= Html::encode($this->title) ?></h1>

            <p>
                <?= Module::t('blog', 'Business inquiries'); ?>
            </p>

            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                <?= $form->field($model, 'subject', ['inputOptions' => ['placeholder' => Module::t('blog', 'Subject')]])->label(false); ?>
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'name', ['inputOptions' => ['placeholder' => Module::t('blog', 'Your Name')]])->label(false); ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'email', ['inputOptions' => ['placeholder' => Module::t('blog', 'Your e-mail')]])->label(false); ?>
                    </div>
                </div>
                <?= $form->field($model, 'body', ['inputOptions' => ['placeholder' => Module::t('blog', 'Message')]])->label(false)->textArea(['rows' => 6]) ?>

                <?= $form->field($model, 'verifyCode')->label(false)->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-sm-3 col-sm-offset-2">{image}</div><div class="col-sm-4 col-sm-offset-1">{input}</div></div>',
                    'captchaAction' => 'blog/captcha',
                ]) ?>

                <div class="form-group text-center">
                    <?= Html::submitButton(Module::t('blog', 'Send'), ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
