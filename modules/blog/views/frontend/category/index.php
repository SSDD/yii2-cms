<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 20.03.2015
 * Time: 19:11
 */

use yii\helpers\Html;
use modules\blog\Module;
use justinvoelker\separatedpager\LinkPager;

$this->title = $model->title . ($pageNum > 1 ? ', '.Module::t('blog', 'page {pageNum}', ['pageNum' => $pageNum]) : '');
$this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="row">
    <aside class="col-sm-4 col-sm-push-8">
        <?= $this->render('/blocks/category_bar') ?>
    </aside>

    <div class="col-sm-8 col-sm-pull-4">
        <div class="category">
            <div class="row">
                <div class="col-sm-2">
                    <span class="in-section"><?= Module::t('blog/category', 'In Section:'); ?></span>
                </div>
                <div class="col-sm-10">
                    <h1><span><?= $model->name; ?></span></h1>
                </div>
            </div>

            <?php if ($model->image || $model->text): ?>
                <div class="row">
                    <?php if ($model->image): ?>
                        <div class="col-sm-2">
                            <?= Html::img(Yii::$app->getModule('blog')->getParam('imagesBaseUrl').'category/'.$model->image, ['class' => 'img-category']); ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($model->text): ?>
                    <div class="col-sm-10">
                        <blockquote>
                            <?= nl2br($model->text); ?>
                        </blockquote>
                    </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>

        <?php
            if(count($subCategories)) {
                echo modules\blog\widgets\sections\Sections::widget(
                    [
                        'newPostsCount' => 0,
                        'onlyCategories' => $subCategories,        // cat. Ids.
                        //'altCategoryNames' => [9=>'про Пол в Доме', 11=>'Тестовый раздел'],  // [$id1=>$name1, $id2=>$name2...]
                        'morePosts' => Module::t('blog/category', 'More posts'), // defaults to false
                    ]
                );
            }
        ?>

        <?php
            foreach ($postModels as $post) {
                echo $this->render('/post/_post_short', [
                    'model' => $post,
                ]);
            }

            // display pagination
            echo LinkPager::widget([
                'options' => ['class' => 'pagination pagination-sm'],
                'pagination' => $pages,
                'maxButtonCount' => 7,
                'nextPageLabel' => false,
                'prevPageLabel' => false,
                'activePageAsLink' => false,
            ]);
        ?>
    </div>
</div>
