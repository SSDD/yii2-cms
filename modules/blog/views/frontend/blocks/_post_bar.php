<?php

use yii\bootstrap\Tabs;
use modules\blog\Module;

?>

<?php
$tabs = [];

if (isset($lastPosts)) {
    $tabs['items'][] =
        [
            'label' => Module::t('blog', 'Last'),
            'content' => $this->render('/blocks/_tab_posts', ['models' => $lastPosts]),
            'options' => ['id' => 'lastPosts'],
            //'active' => true
        ];
}

if (isset($popPosts)) {
    $tabs['items'][] =
        [
            'label' => Module::t('blog', 'Popular'),
            'content' => $this->render('/blocks/_tab_posts', ['models' => $popPosts]),
            'options' => ['id' => 'popularPosts'],
        ];
}

if (isset($lastCommentsArray)) {
    $tabs['items'][] =
        [
            'label' => Module::t('blog', 'Comments'),
            'content' => $this->render('/blocks/_tab_comments', ['commentsArray' => $lastCommentsArray]),
            'options' => ['id' => 'lastComments'],
        ];
}

echo Tabs::widget($tabs);

?>