<?php
use modules\blog\Module;
use modules\blog\widgets\related\RelatedPosts;

$js = <<< JS
var params = {};
    params[$('meta[name=csrf-param]').prop('content')] = $('meta[name=csrf-token]').prop('content');
    $('#post-bar-js').load('/post-bar?tabs=all', params); // empty | all | ?tabs[]=last&tabs[]=popular&tabs[]=comments
JS;

$this->registerJs($js);
?>
<div id="post-bar-js"></div>

<?php if (Yii::$app->getModule('blog')->getParam('isSphinxEnabled')): ?>
    <?= RelatedPosts::widget(
            [
                'blockName' => Module::t('blog/post', 'More on: {categoryName}', ['categoryName'=>$category['crumb']]),
                'postModel' => $model,
                'inCategory'=> true,
                'limit' => Yii::$app->getModule('blog')->getParam('relatedPostsLimit'),
            ]
        );
    ?>

    <?= RelatedPosts::widget(
            [
                'blockName' => Module::t('blog/post', 'Similar Posts'),
                'postModel' => $model,
                'inCategory'=> false,
                'limit' => Yii::$app->getModule('blog')->getParam('relatedPostsLimit'),
            ]
        );
    ?>
<?php endif; ?>