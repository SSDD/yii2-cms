<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use modules\blog\Module;
?>

<?php
$form = ActiveForm::begin([
        'id' => 'search-form',
        'action' => '/search',
        'method' => 'post',
        'options' => [
            'class' => 'navbar-form navbar-left',
            //'style' => 'padding-left:0px;'
        ],
    ]);
?>

<div class="form-group">
    <div class="input-group">
        <div class="input-group-btn">
            <button id="btn-search-dropdown" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
            </button>

            <ul id="site-search" class="dropdown-menu" role="menu">
                <li><a href="#" id="all-categories-search"><?= Module::t('blog', 'In all categories'); ?></a></li>
                <li role="separator" class="divider"></li>
                <?php foreach ($categories as $category): ?>
                    <li><a href="#" category="<?= $category['id']; ?>"><?= $category['crumb']; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>

<!--        --><?/*= Html::input('text', 'search', '', [
            'placeholder' => Module::t('blog', 'Search...'),
            'autocomplete' => 'off',
            'class' => 'form-control'
        ]); */?>

        <?= $form->field($model, 'search', ['template' => '{input}', 'options' => []])
                ->textInput([
                    'placeholder' => Module::t('blog', 'Search...'),
                    'autocomplete' => 'off',
                ]); ?>

        <?= $form->field($model, 'search_category', ['options' => ['id' => 'search-category']])->hiddenInput(['value' => 0])->label(false) ?>

        <span class="input-group-btn">
            <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
        </span>
    </div>
</div>

<?php
ActiveForm::end();

$js = <<<JS
    $("#site-search.dropdown-menu li a").click(function(){
        var itemName = '';
        if ($(this).attr('id') !== 'all-categories-search') {
            itemName = $(this).text();
            //$('#site-menu').hide();
        }
        else {
            //$('#site-menu').show();
        }
        $("#btn-search-dropdown.btn:first-child").html(itemName+' <span class="caret"></span>');
        $("#searchform-search_category").val($(this).attr('category'));
    });
JS;

$this->registerJs($js);
?>