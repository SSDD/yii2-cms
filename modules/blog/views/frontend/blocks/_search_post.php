<?php

use yii\helpers\Html;
use modules\blog\Module;
use modules\blog\components\ContentHelper;

$postUrl = ContentHelper::getPostUrl($model->category_url_id, $model->id, $model->alias);

?>
<article class="post-item">
    <div class="post-short">
        <h3>
            <?= Html::a($model->name, $postUrl) ?>
        </h3>

        <div class="entry-meta">
                <?= $this->render('/blocks/_category_path', [
                    'category_id' => $model->category_id,
                    'startIcon' => '<span class="glyphicon glyphicon-search"></span>'
                ]); ?>
        </div>

        <?= Html::a(
        //Html::img($model->getImageSrc('mini'), ['class' => 'img-thumbnail', 'style' => 'float:left;margin-right:5px;']) .
            Html::img(ContentHelper::getImageSrc('mini', $model->images), ['class' => 'img-thumbnail bar-post-img']),
            $postUrl
        ); ?>
        <?= $snippet; ?>
    </div>
</article>