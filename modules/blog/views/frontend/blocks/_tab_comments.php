<?php
use yii\helpers\Html;
use modules\blog\components\ContentHelper;
?>

<div class="list-group bar-tab">
    <?php foreach ($commentsArray as $comment): ?>
        <div class="list-group-item bar-list-item">
            <div class="bar-comment-img">
                <img class="img-square img-thumbnail" width="60px" src="<?= $comment['gravatar']; ?>" />
                <br>
                <strong>
                    <?= $comment['user_name'] ?>
                </strong>
            </div>

            <div>
                <small class="bar-comment-date">
                    <?= Yii::$app->formatter->asDate($comment['date']) ?>
                </small>
                <br>
                <?php

                    //$postUrl = Url::to(['post/index', 'alias' => $comment['post_alias']]);
                    $postUrl = ContentHelper::getPostUrl($comment['category_url_id'], $comment['post_id'], $comment['post_alias']);

                    echo Html::a(
                        ContentHelper::anons($comment['comment'], 0, 80),
                        $postUrl . '#' .$comment['comment_id'],
                        ['class' => '']
                    );
                ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>
