<?php
use yii\helpers\Html;

$categoryPath = [];

foreach (Yii::$app->categoryTools->breadcrumbs($category_id, '') as $category) {
    $categoryPath[] = Html::a($category['label'], $category['url']);
}
if (count($categoryPath)) {
    echo '<ul class="breadcrumb">' .
        ($startIcon ? '<li>' . $startIcon . '</li>' : '') .
         '<li>' . implode('</li><li>', $categoryPath) . '</li>' .
         '</ul>';
}
