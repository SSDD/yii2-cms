<?php
use yii\helpers\Html;
use modules\blog\components\ContentHelper;
?>

<div class="list-group bar-tab">
    <?php foreach ($models as $model): ?>
        <div class="list-group-item bar-list-item">
            <?= Html::a(
                //Html::img($model->getImageSrc('mini'), ['class' => 'img-thumbnail', 'style' => 'float:left;margin-right:5px;']) .
                Html::img(ContentHelper::getImageSrc('mini', $model->images), ['class' => 'img-thumbnail bar-post-img']) .
                $model->name,
                //['post/index', 'alias' => $model->alias]
                ContentHelper::getPostUrl($model->category_url_id, $model->id, $model->alias)
            ); ?>
        </div>
    <?php endforeach; ?>
</div>
