<?php
use yii\helpers\Html;
use modules\blog\Module;
?>
<div class="row">
    <div class="col-sm-offset-1 col-sm-10">
        <p class="pull-left">
            <?= Html::a(Module::t('blog', 'Rss'), Yii::$app->getModule('blog')->getParam('feed')); ?> &nbsp; | &nbsp;
            <?= Html::a(Module::t('blog', 'Site Map'), ['/blog/blog/sitemap']); ?> &nbsp; | &nbsp;
            <?= Html::a(Module::t('blog', 'Contact Us'), ['/blog/blog/contact']); ?> &nbsp; | &nbsp;
            <?= Html::a(Module::t('blog', 'Privacy Policy'), ['/blog/blog/privacy']); ?>
        </p>
        <p class="pull-right"><small><?= Module::powered() ?></small></p>
        <p class="pull-left"><small>&copy; <?= date('Y') ?> &bull; <?= Module::t('blog', 'All rights reserved'); ?></small></p>
    </div>
</div>
