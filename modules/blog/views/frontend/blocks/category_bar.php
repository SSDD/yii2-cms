<?php
$js = <<< JS
var params = {};
    params[$('meta[name=csrf-param]').prop('content')] = $('meta[name=csrf-token]').prop('content');
    $('#cat-bar-js').load('/post-bar?tabs[]=all', params); // empty | all | ?tabs[]=last&tabs[]=popular&tabs[]=comments
JS;

$this->registerJs($js);
?>
<div id="cat-bar-js"></div>