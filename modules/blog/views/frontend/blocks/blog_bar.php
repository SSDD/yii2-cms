<?php
$js = <<< JS
var params = {};
    params[$('meta[name=csrf-param]').prop('content')] = $('meta[name=csrf-token]').prop('content');
    $('#blog-bar-js').load('/post-bar?tabs[]=popular&tabs[]=comments', params); // empty | all | ?tabs[]=last&tabs[]=popular&tabs[]=comments
JS;

$this->registerJs($js);
?>
<div id="blog-bar-js"></div>