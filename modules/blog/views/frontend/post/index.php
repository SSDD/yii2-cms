<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 01.05.2015
 * Time: 13:34
 */
use modules\blog\Module;
use modules\blog\components\ContentHelper;
use modules\blog\models\Post;

// meta description and meta keywords are shown on the first page only
if (!$pageNum) {
    // meta description
    $description = $model->description ? $model->description : ContentHelper::anons($model->text);
    if ($description) {
        $this->registerMetaTag(['description' => $description]);
    }

    // meta keywords
    if ($model->keywords) {
        $this->registerMetaTag(['description' => $model->keywords]);
    }
}

$this->title = $model->title . ($pageNum > 1 ? ', '.Module::t('blog', 'page {pageNum}', ['pageNum' => $pageNum]) : '');
$this->params['breadcrumbs'] = $breadcrumbs;
?>
<div class="row">
    <aside class="col-sm-4 col-sm-push-8">
        <?= $this->render('/blocks/post_bar', ['model' => $model, 'category' => $category]) ?>
    </aside>

    <div class="col-sm-8 col-sm-pull-4">
        <?php
        if (!$pageNum) { ?>
            <div class="post-item">
                <h1><?= $model->name; ?></h1>
                <?= ContentHelper::insertAd($model->text); ?>
            </div>

            <div id="social-share" class="post-item text-center"></div>
            <?php
                $js = <<< JS
                var params = {};
                params[$('meta[name=csrf-param]').prop('content')] = $('meta[name=csrf-token]').prop('content');
                $('#social-share').load('/social', params);
JS;
                $this->registerJs($js);
            ?>

        <?php
        } else {
            // for pages with comments
            echo $this->render('_post_page', [
                'model' => $model,
                'pageNum' => $pageNum
            ]);
        }
        ?>

        <?php
            // Comments widget
            if ($this->context->module->getParam('blogCommentsStatus') !== Module::BLOG_COMMENTS_HIDDEN &&
                $model->comments_state !== Post::COMMENTS_HIDDEN
            ) {
                echo modules\blog\widgets\comments\Comments::widget(
                    [
                        'post_id' => $model->id,
                        'comments_state' => $model->comments_state,
                        'pageNum' => $pageNum,
                        'commentCount' => $model->commentCount,
                        'redirectUrlParams' => [
                            'blog/post/index',
                            'alias' => $model->alias
                        ]
                    ]
                );
            }
        ?>
    </div>
</div>
<?= newerton\fancybox\FancyBox::widget([
    'target' => 'a[rel=fancybox]',
    'helpers' => true,
    'mouse' => true,
    'config' => [
        'maxWidth' => '90%',
        'maxHeight' => '90%',
        //'playSpeed' => 7000,
        //'padding' => 0,
        'padding' => 10,
        'fitToView' => false,
        'width' => '70%',
        'height' => '70%',
        'autoSize' => false,
        'closeClick' => false,
        'openEffect' => 'elastic',
        'closeEffect' => 'elastic',
        'prevEffect' => 'elastic',
        'nextEffect' => 'elastic',
        'closeBtn' => true,
        'openOpacity' => true,
        'helpers' => [
            //'title' => ['type' => 'float'],
            'title' => ['type' => 'over'],
            //'buttons' => [],
            //'thumbs' => ['width' => 68, 'height' => 50],
            'overlay' => [
                'css' => [
                    'background' => 'rgba(0, 0, 0, 0.8)'
                ]
            ]
        ],
        'tpl' => [
            'next' => '<a title="'.Module::t('blog', 'Next').'" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
            'prev' => '<a title="'.Module::t('blog', 'Previous').'" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>',
        ],
    ]
]);
