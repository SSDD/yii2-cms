<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 24.06.2015
 * Time: 13:31
 */

use yii\helpers\Html;
use modules\blog\Module;
use modules\blog\components\ContentHelper;
use modules\blog\models\Post;

?>
<article class="post-item">
    <div class="post-short">
        <h3>
            <?= $model->name . ' ' . Module::t('blog/post', '(Page {pageNum})', ['pageNum' => $pageNum]); ?>
        </h3>

        <?php if ( $model->commentCount &&
                   $this->context->module->getParam('blogCommentsStatus') !== Module::BLOG_COMMENTS_HIDDEN &&
                   $model->comments_state !== Post::COMMENTS_HIDDEN
        ) : ?>
            <div class="entry-meta">
            <span>
                <?= Module::t('blog/comment', 'comments: ({commentCount})', [
                    'commentCount' => Html::a($model->commentCount, '#comments')
                ]);
                ?>
            </span>
            </div>
        <?php endif; ?>

        <?php
        echo $model->anons;
        ?>
    </div>
</article>