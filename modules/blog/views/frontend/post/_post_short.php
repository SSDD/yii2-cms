<?php

use yii\helpers\Html;
use modules\blog\Module;
use modules\blog\components\ContentHelper;
use modules\blog\models\Post;

$postUrl = ContentHelper::getPostUrl($model->category_url_id, $model->id, $model->alias);

?>
<article class="post-item">
    <div class="post-short">
        <h3>
            <?= Html::a($model->name, $postUrl) ?>
        </h3>

        <?php if ($model->commentCount && $this->context->module->getParam('blogCommentsStatus') !== Module::BLOG_COMMENTS_HIDDEN &&
                  $model->comments_state !== Post::COMMENTS_HIDDEN
        ) : ?>
        <div class="entry-meta">
            <span>
                <?= Module::t('blog/comment', 'comments: ({commentCount})', [
                    'commentCount' => Html::a($model->commentCount, $postUrl . '#comments')
                ]);
                ?>
            </span>
        </div>
        <?php endif; ?>

        <?php
            echo $model->anons;
        ?>
    </div>
</article>