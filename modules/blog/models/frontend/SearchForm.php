<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 22.06.2015
 * Time: 15:44
 */

namespace modules\blog\models\frontend;

use yii\base\Model;

/**
 * Password reset request form
 */
class SearchForm extends Model
{
    public $search;
    public $search_category;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['search'], 'string', 'length' => [3, 100]],
            //[['search_category'], 'required'],
            [['search_category'], 'integer'],
        ];
    }
}