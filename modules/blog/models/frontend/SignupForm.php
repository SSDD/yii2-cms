<?php
namespace modules\blog\models\frontend;

use Yii;
use yii\base\Model;
use modules\blog\Module;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $fullname;
    public $username;
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fullname', 'username'], 'filter', 'filter' => 'trim'],
            [['fullname', 'username'], 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->fullname = $this->fullname;
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }

    public function attributeLabels() {
        return [
            'username' => Module::t('blog', 'Username'),
            'fullname' => Module::t('blog', 'Full Name'),
            'email' => Module::t('blog', 'Your e-Mail'),
            'password' => Module::t('blog', 'Password')
        ];
    }
}
