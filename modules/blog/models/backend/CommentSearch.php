<?php

namespace modules\blog\models\backend;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\blog\models\Comment;

/**
 * CommentSearch represents the model behind the search form about `modules\blog\models\Comment`.
 */
class CommentSearch extends Comment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'comment_id', 'user_id', 'user_name'], 'integer'],
            ['status', 'in', 'range' => [Comment::PENDING, Comment::APPROVED, Comment::BANNED]],
            [['post_id', 'comment', 'date', 'ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

//    public function beforeValidate() {
//        return Model::beforeValidate();
//    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Comment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // postLookup - is the name of the relationship
        $query->joinWith('postLookup');

        $query->andFilterWhere([
            'id' => $this->id,
            //'post_id' => $this->post_id,
            'comment_id' => $this->comment_id,
            'user_id' => $this->user_id,
            'user_name' => $this->user_name,
            'comment.status' => $this->status,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment])
            //->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'post.name', $this->post_id]);

        return $dataProvider;
    }
}
