<?php

namespace modules\blog\models\backend;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\blog\models\Category;
//use modules\blog\models\Post;
//use yii\db\Query;

/**
 * CategorySearch represents the model behind the search form about `modules\blog\models\Category`.
 */
class CategorySearch extends Category
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_id'], 'integer'],
            [['title', 'name', 'alias', 'crumb', 'snippet', 'text', 'image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
//        $subQuery = new Query();
//        $subQuery
//            ->select('COUNT(p.id)')
//            ->from(Post::tableName().' p')
//            ->where(['p.category_id' => 'category.id']);
//        $command = $subQuery->createCommand();

//        $query = Category::find()
//            ->select([
//                Category::tableName().'.*',
//                '(SELECT COUNT(post.id) FROM post WHERE post.category_id = category.id AND post.status = "'.Post::POST_PUBLISHED.'") as postCount'
//            ]);
        $query = Category::query();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'parent_id' => $this->parent_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'crumb', $this->crumb])
            ->andFilterWhere(['like', 'snippet', $this->snippet])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'image', $this->image]);

        $query->orderBy('order');

        return $dataProvider;
    }
}
