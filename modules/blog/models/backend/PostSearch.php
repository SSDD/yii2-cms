<?php

namespace modules\blog\models\backend;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\blog\models\Post;

/**
 * PostSearch represents the model behind the search form about `modules\blog\models\Post`.
 */
class PostSearch extends Post
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'visits'], 'integer'],
            ['status', 'in', 'range' => [Post::POST_DRAFT, Post::POST_PUBLISHED]],
            ['comments_state', 'in', 'range' => [self::COMMENTS_ON, self::COMMENTS_OFF, self::COMMENTS_HIDDEN]],
            [['user_id', 'date', 'title', 'keywords', 'name', 'alias', 'description', 'anons', 'text', 'images', 'tags'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Post::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // userLookup - is the name of the relationship
        $query->joinWith('userLookup');

        //echo $this->status;die;

        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            //'user_id' => $this->user_id,
            'date' => $this->date,
            'post.status' => $this->status,
            'post.comments_state' => $this->comments_state,
            'visits' => $this->visits,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'keywords', $this->keywords])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'anons', $this->anons])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'images', $this->images])
            ->andFilterWhere(['like', 'tags', $this->tags])
            //->andFilterWhere(['like', 'post.status', $this->status])
            ->andFilterWhere(['like', 'user.username', $this->user_id]);

        return $dataProvider;
    }
}
