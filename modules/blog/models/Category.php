<?php

namespace modules\blog\models;

use Yii;
use yii\helpers\ArrayHelper;
use modules\blog\Module;
use himiklab\sortablegrid\SortableGridBehavior;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $title
 * @property string $name
 * @property string $alias
 * @property string $label
 * @property string $snippet
 * @property string $text
 * @property string $image
 * @property integer $posts_count
 */
class Category extends \yii\db\ActiveRecord {

    const CACHE_CATEGORIES_ARRAY = 'cache_categories_array';
    const CACHE_CATEGORY_MODEL = 'cache_category_model';

    public $imageFile;
    public $imageRemove;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'category';
    }

    public $postCount;

    public function behaviors() {
        return [
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'order'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['order'], 'safe'],
            [['parent_id'], 'integer'],
            [['parent_id'], 'default', 'value' => 0],
            ['parent_id', 'parentIdRule'],
            [['name', 'alias', 'crumb'], 'required'],
            [['snippet', 'text'], 'string'],
            [['title', 'name', 'alias', 'crumb', 'image'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'extensions' => ['png', 'jpg', 'gif']],
            ['imageRemove', 'boolean'],
            [['name'], 'unique'],
            [['alias'], 'unique'],
            [['parent_id', 'name'], 'unique', 'targetAttribute' => ['parent_id', 'name'], 'message' => 'The combination of Parent ID and Name has already been taken.'],
            [['parent_id', 'alias'], 'unique', 'targetAttribute' => ['parent_id', 'alias'], 'message' => 'The combination of Parent ID and Alias has already been taken.'],
        ];
    }

    public function parentIdRule($attribute) {
        $valid = true;

        if ($this->isNewRecord) {
            return $valid;
        }

        $categories = self::getCategories();

        foreach ($categories as $category) {
            if ($category['id'] == $this->parent_id && $category['parent_id'] == $this->id) {
                $this->addError($attribute, 'You are trying to set a Child as a Parent');
                $valid = false;
                break;
            }
        }
        return $valid;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Module::t('blog', 'ID'),
            'parent_id' => Module::t('blog/category', 'Parent ID'),
            'title' => Module::t('blog', 'Title'),
            'name' => Module::t('blog/category', 'Category Name'),
            'alias' => Module::t('blog/category', 'Alias'),
            'crumb' => Module::t('blog/category', 'Category Crumb'),
            'snippet' => Module::t('blog/category', 'Snippet'),
            'text' => Module::t('blog', 'Content'),
            'image' => Module::t('blog/category', 'Image'),
            'imageFile' => Module::t('blog/category', 'Logo'),
            'imageRemove' => Module::t('blog/category', 'Remove Logo'),
            'postCount' => Module::t('blog/category', 'Posts Count')
        ];
    }

    public static function getCategories() {
        if (($categories = Yii::$app->cache->get(self::CACHE_CATEGORIES_ARRAY)) === false) {
            $categories = ArrayHelper::map(
                //self::find()->orderBy('order')->all(),
                self::query()->orderBy('order')->all(),
                'id',
                function($el) {
                    return [
                        'id' => $el['id'],
                        'order' => $el['order'],
                        'parent_id' => $el['parent_id'],
                        'name' => $el['name'],
                        'crumb' => $el['crumb'],
                        'alias' => $el['alias'],
                        'postCount' => $el['postCount'],
                    ];
                }
            );
            Yii::$app->cache->set(self::CACHE_CATEGORIES_ARRAY, $categories, Yii::$app->getModule('blog')->getParam('cacheDuration'));
        }

        return $categories;
    }

    public static function query() {
        return self::find()
            ->select([
                Category::tableName().'.*',
                '(SELECT COUNT(post.id) FROM post WHERE post.category_id = category.id AND post.status = "'.Post::POST_PUBLISHED.'") as postCount'
            ]);
    }

    public static function clearCache($alias = null) {
        if ($alias && Yii::$app->cache->exists(self::CACHE_CATEGORY_MODEL . $alias)) {
            Yii::$app->cache->delete(self::CACHE_CATEGORY_MODEL . $alias);
        }

        if (Yii::$app->cache->exists(self::CACHE_CATEGORIES_ARRAY)) {
            Yii::$app->cache->delete(self::CACHE_CATEGORIES_ARRAY);
        }
    }

}
