<?php

namespace modules\blog\models;

use Yii;
use modules\blog\Module;
use modules\blog\components\ContentHelper;
use yii\helpers\HtmlPurifier;
use yii\db\Query;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property integer $post_id
 * @property integer $comment_id
 * @property integer $user_id
 * @property integer $user_name
 * @property string $email
 * @property string $comment
 * @property string $date
 * @property string $status
 * @property string $ip
 *
 * @property Post $post
 */
class Comment extends \yii\db\ActiveRecord {

    const PENDING = 'pending';
    const APPROVED = 'approved';
    const BANNED = 'banned';

    const CACHE_POST_COMMENTS = 'cache_post_models_';
    const CACHE_LAST_COMMENTS = 'cache_last_comments_';

    public $user_role;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'comment';
    }

    public function scenarios () {
        $scenarios = parent::scenarios();
        $scenarios['backend'] = ['post_id', 'date', 'user_role', 'user_id', 'user_name', 'email', 'comment', 'status'];
        $scenarios['frontend'] = ['user_name', 'email', 'comment', 'user_role'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['post_id', 'comment', 'status', 'email'], 'required'],
            [['user_id'], 'default', 'value' => 0],
            [ // user_id
                'user_id', 'required', 'when' => function($model) {
                    return $model->user_role == '2';
                },'whenClient' => "function (attribute, value) {
                    //alert($('#user-role .active').find('input').val());
                    return $('#user-role .active').find('input').val() == '2';
                }",
                'message' => Module::t('blog/comment', 'User name is required.')
            ],
            [ // user_name
                'user_name', 'required', 'when' => function ($model) {
                    // when user is guest
                    //return Yii::$app->user->isGuest;
                    return $model->user_role == '1';
                }, 'whenClient' => "function (attribute, value) {
                    //alert($('#user-role .active').find('input').val());
                    return $('#user-role .active').find('input').val() == '1';
                }"
            ],
            [['post_id', 'comment_id', 'user_id'], 'integer'],
            [['comment', 'status'], 'string'],
            ['email', 'email'],
            [['date'], 'safe'],
            [['user_name'], 'string', 'max' => 255],
            ['status', 'default', 'value' => self::PENDING],
            ['status', 'in', 'range' => [self::PENDING, self::APPROVED, self::BANNED]],
            [['ip'], 'string', 'max' => 15],
            [['post_id', 'comment_id'], 'unique', 'targetAttribute' => ['post_id', 'comment_id'], 'message' => 'The combination of Post ID and Comment ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Module::t('blog/comment', 'ID'),
            'post_id' => Module::t('blog/comment', 'Post ID'),
            'comment_id' => Module::t('blog/comment', 'Comment ID'),
            'user_id' => Module::t('blog/comment', 'User ID'),
            'user_name' => Module::t('blog/comment', 'User Name'),
            'email' => Module::t('blog/comment', 'User Email'),
            'comment' => Module::t('blog/comment', 'Comment'),
            'date' => Module::t('blog/comment', 'Date'),
            'status' => Module::t('blog/comment', 'Status'),
            'statusName' => Module::t('blog/comment', 'Status Name'),
            'ip' => Module::t('blog/comment', 'Ip'),
        ];
    }

    public function setUserById() {
        $ok = true;
        if ($this->user_id) {
            if ($userModel = \common\models\User::findOne(['id' => $this->user_id])) {
                $this->setAttribute('user_name', $userModel->username);
                $this->setAttribute('email', $userModel->email);
            }
            else {
                $ok = false;
            }
        }
        return $ok;
    }

    public function getStatusName() {
        $statusArray = self::getStatusArray();
        return $statusArray[$this->status];
    }

    /**
     * @return array Status array.
     */
    public static function getStatusArray() {
        return [
            self::PENDING => Module::t('blog/comment', 'Pending'),
            self::APPROVED => Module::t('blog/comment', 'Approved'),
            self::BANNED => Module::t('blog/comment', 'Banned'),
        ];
    }

    public static function lastCommentsArray() {
        if (($commentsArray = Yii::$app->cache->get(self::CACHE_LAST_COMMENTS)) === false) {
            $query = new Query;
            $commentsArray = $query->select(
                        [
                            'c.id',
                            'c.post_id',
                            'c.comment_id as comment_id',
                            'c.user_name as user_name',
                            'c.date',
                            'c.user_name',
                            'c.email',
                            'c.comment',
                            'p.alias as post_alias',
                            'p.category_url_id',
                        ]
                    )
                    ->from(
                        [
                            Post::tableName().' p',
                            Comment::tableName().' c'
                        ]
                    )
                    ->andWhere('p.id = c.post_id')
                    ->andWhere('c.id = ( SELECT max(c2.id) '.
                        'FROM comment c2 '.
                        'WHERE c2.post_id = p.id AND '.
                        'c2.status = "'.Comment::APPROVED.'" AND '.
                        'p.status = "'.Post::POST_PUBLISHED.'" '.
                        'ORDER BY c2.id DESC '.
                        'LIMIT 1)')
                    ->limit(Yii::$app->getModule('blog')->getParam('commentsPerSidebar'))
                    ->orderBy(['c.id' => SORT_DESC])
                    ->all();

            foreach($commentsArray as $id=>$comment) {
                $commentsArray[$id]['gravatar'] = ContentHelper::getGravatar(
                    $comment['email'],
                    80,
                    'wavatar'
                );
            }

            Yii::$app->cache->set(self::CACHE_LAST_COMMENTS, $commentsArray, Yii::$app->getModule('blog')->getParam('cacheDuration'));
        }
        return $commentsArray;
    }

    public static function clearCache($post_id) {
        $cachePageNum = 0;
        while (Yii::$app->cache->exists(self::CACHE_POST_COMMENTS . $post_id . '_' . (++$cachePageNum))) {
            Yii::$app->cache->delete(self::CACHE_POST_COMMENTS . $post_id . '_' . $cachePageNum);
        }

        if (Yii::$app->cache->exists(self::CACHE_LAST_COMMENTS)) {
            Yii::$app->cache->delete(self::CACHE_LAST_COMMENTS);
        }

        if ($model = Post::findOne($post_id)) {
            Post::clearCache($model->alias);
        }
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostLookup() {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }

    public function getPostName() {
        return HtmlPurifier::process($this->postLookup->name);
    }

    public function getPostAlias() {
        return HtmlPurifier::process($this->postLookup->alias);
    }

    public function getMaxCommentId() {
        $max_id = self::find()->andWhere(['post_id' => $this->post_id])->select('comment_id')->max('comment_id');
        return $max_id ? $max_id : 0;
    }

    public function beforeSave($insert) {
        //die($this->date);
        if ($insert) {
            $this->setAttribute('comment_id', $this->maxCommentId+1);
        }
        return parent::beforeSave($insert);
    }
}
