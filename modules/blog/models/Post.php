<?php

namespace modules\blog\models;
use common\models\User;
use modules\blog\Module;
use modules\blog\components\RtSphinxBehavior;

use Yii;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\helpers\Json;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Image\ImageInterface;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $category_url_id
 * @property integer $user_id
 * @property string $date
 * @property string $title
 * @property string $keywords
 * @property string $name
 * @property string $alias
 * @property string $description
 * @property string $anons
 * @property string $text
 * @property string $images
 * @property string $tags
 * @property enum $status
 * @property integer $visits
 * @property enum $comments_state
 */
class Post extends \yii\db\ActiveRecord {
    const POST_DRAFT = 'draft';
    const POST_PUBLISHED = 'published';

    const COMMENTS_ON = 'on';
    const COMMENTS_OFF = 'off';
    const COMMENTS_HIDDEN = 'hidden';

    const CACHE_POST_MODEL = 'cache_post_model';
    const CACHE_LAST_POSTS = 'cache_last_posts';
    const CACHE_POP_POSTS = 'cache_pop_posts';
    const CACHE_REL_POSTS = 'cache_rel_posts';

    public $image;
    public $crop_info;

    public $commentCount = 0;

    public function behaviors () {
        return [
            'rtSphinxBehavior' => [
                'class' => RtSphinxBehavior::className(),
                'rtIndex' => Yii::$app->getModule('blog')->getParam('sphinxRtIndex'),
                'idAttributeName' => 'id',
                'rtFieldNames' => ['name', 'title', 'description', 'text'],
                'rtAttributeNames' => ['category_id'],
                'enabled' => Yii::$app->getModule('blog')->getParam('isSphinxEnabled'),
            ],
        ];
    }

    public static function tableName() {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['visits'], 'integer'],
            ['status', 'default', 'value' => self::POST_DRAFT],
            ['status', 'in', 'range' => [self::POST_DRAFT, self::POST_PUBLISHED]],
            ['comments_state', 'default', 'value' => self::COMMENTS_ON],
            ['comments_state', 'in', 'range' => [self::COMMENTS_ON, self::COMMENTS_OFF, self::COMMENTS_HIDDEN]],
            [['date', 'user_id', 'text', 'category_url_id'], 'safe'],
            [['title', 'name', 'alias', 'anons', 'text'], 'required'],
            ['category_id', 'required', 'message' => Module::t('blog', 'Required Field')],
            [['anons', 'text', 'images', 'tags'], 'string'],
            [['title', 'keywords', 'name', 'alias', 'description'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['alias'], 'unique'],
            [
                'image',
                'file',
                'extensions' => ['jpg', 'jpeg', 'png', 'gif'],
                'mimeTypes' => ['image/jpeg', 'image/pjpeg', 'image/png', 'image/gif']
            ],
            ['crop_info', 'safe']
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['cropMe'] = ['crop_info', 'image'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Module::t('blog/post', 'ID'),
            'category_id' => Module::t('blog/post', 'Category ID'),
            'categoryName' => Module::t('blog/post', 'Category Name'),
            'user_id' => Module::t('blog/post', 'User ID'),
            'date' => Module::t('blog/post', 'Date'),
            'title' => Module::t('blog/post', 'Title'),
            'keywords' => Module::t('blog/post', 'Keywords'),
            'name' => Module::t('blog/post', 'Name'),
            'alias' => Module::t('blog/post', 'Alias'),
            'description' => Module::t('blog/post', 'Description'),
            'anons' => Module::t('blog/post', 'Anons'),
            'text' => Module::t('blog/post', 'Text'),
            'images' => Module::t('blog/post', 'Images'),
            'tags' => Module::t('blog/post', 'Tags'),
            'status' => Module::t('blog/post', 'Status Name'),
            'statusName' => Module::t('blog/post', 'Status Name'),
            'commentsState' => Module::t('blog/post', 'Comments State'),
            'commentsStateName' => Module::t('blog/post', 'Comments State'),
            'visits' => Module::t('blog/post', 'Visits'),
            'image' => '',
        ];
    }

    public static function clearCache($alias) {
        $keys = [
            self::CACHE_POST_MODEL . $alias,
            self::CACHE_LAST_POSTS,
            self::CACHE_POP_POSTS,
            self::CACHE_REL_POSTS . $alias . '_0',
            self::CACHE_REL_POSTS . $alias . '_1',
            Comment::CACHE_LAST_COMMENTS
        ];

        foreach ($keys as $key) {
            if (Yii::$app->cache->exists($key)) {
                Yii::$app->cache->delete($key);
            }
        }
    }

    public function getCategoryLookup() {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getCategoryName() {
        return $this->categoryLookup->name;
    }

    public function getUserLookup() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserName() {
        return $this->userLookup->username;
    }

    public function getStatusName() {
        $statusArray = self::getStatusArray();
        return $statusArray[$this->status];
    }

    /**
     * @return array Status array.
     */
    public static function getStatusArray() {
        return [
            self::POST_DRAFT => Module::t('blog/post', 'Is Draft'),
            self::POST_PUBLISHED => Module::t('blog/post', 'Is Published'),
        ];
    }

    public function getCommentsStateName() {
        $stateArray = self::getCommentsStateArray();
        return $stateArray[$this->comments_state];
    }

    /**
     * @return array Status array.
     */
    public static function getCommentsStateArray() {
        return [
            self::COMMENTS_ON => Module::t('blog/post', 'Comments On'),
            self::COMMENTS_OFF => Module::t('blog/post', 'Comments Off'),
            self::COMMENTS_HIDDEN => Module::t('blog/post', 'Comments Hidden'),
        ];
    }

    public static function lastPosts() {
        if (($lastPosts = Yii::$app->cache->get(self::CACHE_LAST_POSTS)) === false) {
            if (($lastPosts = self::find()
                    ->andWhere(['status' => self::POST_PUBLISHED])
                    ->orderBy(['id' => SORT_DESC])
                    ->limit(Yii::$app->getModule('blog')->getParam('postsPerSidebar'))
                    ->all()) !== null) {
                Yii::$app->cache->set(self::CACHE_LAST_POSTS, $lastPosts, Yii::$app->getModule('blog')->getParam('cacheDuration'));
            }
        }
        return $lastPosts;
    }

    public static function popularPosts() {
        if (($popPosts = Yii::$app->cache->get(self::CACHE_POP_POSTS)) === false) {
            if (($popPosts = self::find()
                    ->select([
                        self::tableName().'.*',
                        '(SELECT COUNT(com.post_id)'.
                        ' FROM '.Comment::tableName().' com'.
                        ' WHERE com.post_id = post.id AND com.status="'.Comment::APPROVED.'") as commentCount'
                    ])
                    ->andWhere(['status' => self::POST_PUBLISHED])
                    ->orderBy(['commentCount' => SORT_DESC])
                    ->limit(Yii::$app->getModule('blog')->getParam('postsPerSidebar'))
                    ->all()) !== null) {
                Yii::$app->cache->set(self::CACHE_POP_POSTS, $popPosts, Yii::$app->getModule('blog')->getParam('cacheDuration'));
            }
        }
        return $popPosts;
    }

    public static function serializeImages($imagesArray) {
        $images = '';
        if (count($imagesArray)) {
            rsort($imagesArray);
            $images = serialize($imagesArray);
        }
        else {
            $images = '';
        }
        return $images;
    }

    public function afterSave($insert, $changedAttributes)  {
        if (isset($this->image->tempName)) {
            $imageQuality = Yii::$app->getModule('blog')->getParam('imageQuality');

            $pathImage = Yii::getAlias('@images');
            $postImages = FileHelper::findFiles($pathImage, ['only' => [$this->id . '-*.*'], 'recursive' => false]);

            // Get rid of the path
            foreach ($postImages as $i => $file) {
                $file = str_replace($pathImage, '', $file);
                $file = stripslashes($file);
                $file = str_replace('/', '', $file);
                $postImages[$i] = $file;
            }

            $imageName = $this->id . '-' . $this->alias;

            if (in_array($imageName . '.' . $this->image->getExtension(), $postImages)) {
                for ($i = 1; ; $i++) {
                    if (!in_array($imageName . '-' . $i . '.' . $this->image->getExtension(), $postImages)) {
                        $imageName .= '-' . $i . '.' . $this->image->getExtension();
                        $postImages[] = $imageName;
                        break;
                    }
                }
            } else {
                $imageName .= '.' . $this->image->getExtension();
                $postImages[] = $imageName;
            }

            $image = Image::getImagine()->open($this->image->tempName);

            if (!empty($this->crop_info)) {
                // rendering information about crop of ONE option
                $cropInfo = Json::decode($this->crop_info)[0];
                $cropInfo['dw'] = (int)$cropInfo['dw']; //new width image
                $cropInfo['dh'] = (int)$cropInfo['dh']; //new height image
                $cropInfo['x'] = abs($cropInfo['x']); //begin position of frame crop by X
                $cropInfo['y'] = abs($cropInfo['y']); //begin position of frame crop by Y
                //$cropInfo['ratio'] = $cropInfo['ratio'] == 0 ? 1.0 : (float)$cropInfo['ratio']; //ratio image.
                $cropInfo['w'] = (int)$cropInfo['w']; //width of cropped image
                $cropInfo['h'] = (int)$cropInfo['h']; //height of cropped image

                // saving post-image
                $newSize = new Box($cropInfo['dw'], $cropInfo['dh']);
                $cropSize = new Box($cropInfo['w'], $cropInfo['h']); //frame size of crop
                $cropPoint = new Point($cropInfo['x'], $cropInfo['y']);
                $image->resize($newSize)
                    ->crop($cropPoint, $cropSize)
                    ->save($pathImage . '/' . $imageName, ['quality' => $imageQuality]);
            } else {

                $w = Yii::$app->getModule('blog')->getParam('imgPostWidth');
                $h = Yii::$app->getModule('blog')->getParam('imgPostHeight');

                $image->thumbnail(new Box($w, $h), ImageInterface::THUMBNAIL_OUTBOUND)->save($pathImage . '/' . $imageName, ['quality' => $imageQuality]);
            }

            // saving thumbnail-image
            $w = Yii::$app->getModule('blog')->getParam('imgThumbWidth');
            $h = Yii::$app->getModule('blog')->getParam('imgThumbHeight');

            $image->thumbnail(new Box($w, $h), ImageInterface::THUMBNAIL_OUTBOUND)->save($pathImage . '/thumb/' . $imageName, ['quality' => $imageQuality]);

            // saving mini-thumbnail-image
            $w = Yii::$app->getModule('blog')->getParam('imgMiniWidth');
            $h = Yii::$app->getModule('blog')->getParam('imgMiniHeight');

            $image->thumbnail(new Box($w, $h), ImageInterface::THUMBNAIL_OUTBOUND)->save($pathImage . '/mini/' . $imageName, ['quality' => $imageQuality]);

            // saving full-image
            $w = Yii::$app->getModule('blog')->getParam('imgFullWidth');
            $h = Yii::$app->getModule('blog')->getParam('imgFullHeight');

            $imageFull = Image::getImagine()->open($this->image->tempName);

            if ($imageFull->getSize()->getWidth() < $w) {
                $this->image->saveAs($pathImage . '/full/' . $imageName);
            } else {// saving original-image
                $imageFull->thumbnail(new Box($w, $h), ImageInterface::THUMBNAIL_OUTBOUND)->save($pathImage . '/full/' . $imageName, ['quality' => $imageQuality]);
            }

            $this->setAttribute('images', self::serializeImages($postImages));
            Yii::$app->db->createCommand("UPDATE post SET images=:images WHERE id=:id")->bindValues([':id' => $this->id, ':images' => $this->images])->execute();
        }

        parent::afterSave($insert, $changedAttributes);
    }
}
