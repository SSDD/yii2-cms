<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 12.05.2015
 * Time: 12:37
 */
return [
    'Comments' => 'Комментарии',
    'Create Comment' => 'Добавить комментарий',
    'Leave a Comment' => 'Оставьте комментарий ',
    'Send comment' => 'Отправить',
    'Date' => 'Дата',
    'Comment' => 'Текст комментария',
    'Status' => 'Статус',
    'dated {date}' => 'от {date}',
    'in {time}' => 'в {time}',
    'comments: ({commentCount})' => 'комментариев: ({commentCount})',
    'User Role' => 'Роль',
    'User Name' => 'Имя',
    'User Email' => 'Email',
    'Enter your name...' => 'Представьтесь, пожалуйста...',
    'Enter your email...' => 'Укажите свой email (не публикуется)...',
    'Comment text...' => 'Текст комментария...',
    'Update Comment' => 'Изменить комментарий',
    'User name is required.' => 'Необходимо заполнить «Имя».',
    'The comment has been added.' => 'Спасибо за Ваш комментарий.',
    'Please, wait while moderation is in progress.' => 'Опубликуем сразу после модерации',
];