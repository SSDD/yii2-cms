<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 27.03.2015
 * Time: 11:47
 */
return [
    'Posts' => 'Статьи',
    'Category Name' => 'Категория',
    'Select Category' => 'Выберите категорию...',
    'Select Status' => 'Выберите статус...',
    'Create Post' => 'Добавить статью',
    'New Post' => 'Новая статья',
    'Update {modelClass}: ' => 'Изменение:',
    'Author Name' => 'Имя автора',
    'Date' => 'Дата',
    'Tags' => 'Метки',
    'Title' => 'Meta Title',
    'Keywords' => 'Meta Keywords',
    'Description' => 'Meta Description',
    'Name' => 'Название статьи',
    'Alias' => 'Алиас',
    'Anons' => 'Анонс',
    'Text' => 'Текст статьи',
    'Status Name' => 'Статус',
    'Is Published' => 'Опубликована',
    'Is Draft' => 'Черновик',
    'More on: {categoryName}' => 'Ещё по теме &laquo{categoryName}&raquo;',
    'Similar Posts' => 'Похожие статьи',
];