<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 27.03.2015
 * Time: 11:47
 */
return [
    'Categories' => 'Категории',
    'categories' => 'категории',
    'Category' => 'Категория',
    'Category in url' => 'Категория в url',
    'In Section:' => 'В разделе:',
    'Parent ID' => 'Родительская категория',
    'Select Category' => 'Выберите категорию...',
    'Category Crumb' => 'Хлебная крошка',
    'Category Name' => 'Название категории',
    'Alias' => 'Алиас',
    'Posts Count' => 'Статей',
    'Snippet' => 'Краткое описание',
    'Image Url' => 'Картинка',
    'Create Category' => 'Добавить категорию',
    'New Category' => 'Новая категория',
    'Search' => 'Поиск',
    'Reset' => 'Сбросить',
    'Update {modelClass}: ' => 'Изменение:',
    'Are you sure you want to delete this item?' => 'Вы уверены, что хотите удалить эту запись?',
    'More posts' => 'Все статьи <span class="glyphicon glyphicon-menu-right" style="top:2px;font-size:12px;"></span>',
];