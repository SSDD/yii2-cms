<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 24.03.2015
 * Time: 15:31
 */

return [
    // Blog
    'Authentication' => 'Авторизация',
    'Login' => 'Вход',
    'Signup' => 'Регистрация',
    'Contact Us' => 'Контакты',
    'Please fill out the following fields to login:' => 'Для авторизации, пожалуйста, заполните поля формы:',
    'Please fill out the following fields to signup:' => 'Заполните, пожалуйста, поля формы:',
    'Username' => 'Логин',
    'Full Name' => 'Полное имя',
    'Password' => 'Пароль',
    'If you forgot your password you can {reset it}.' => 'Для восстановления пароля перейдте по {reset it}.',
    'reset it' => 'ссылке',
    'Request password reset' => 'Восстановления пароля',
    'Request form' => 'Форма запроса',
    'Reset password' => 'Сброс пароля',
    'Please choose your new password:' => 'Укажите, пожалуйста, новый пароль:',
    'Please fill out your email. A link to reset password will be sent there.' => 'Укажите, пожалуйста, e-Mail для получения ссылки для восстановления пароля.',
    'There is no user with such email.' => 'Пользователь с указанным e-Mail не найден.',
    'Check your email for further instructions.' => 'Проверьте почту для дальнейших инструкций',
    'Sorry, we are unable to reset password for email provided.' => 'Приносим извинения, но отправка на указанный e-Mail не возможна.',
    'New password was saved.' => 'Установлен новый пароль.',
    'Remember Me' => 'Запомнить меня',
    'Incorrect username or password.' => 'Проверьте правильность ввода полей "Логин" и "Пароль".',
    'About' => 'О сайте',
    'Contact Us' => 'Контакты',
    'Site Map' => 'Карта сайта',
    'Privacy Policy' => 'Политика конфиденциальности',
    'All rights reserved' => 'Материалы сайта защищены Законом РФ "Об авторском праве".',
    'Contact Page' => 'Обратная связь',
    'Contact Form' => 'Форма обратной связи',
    'Business inquiries' => 'Мы открыты для Ваших бизнес предложений, вопросов, пожеланий.',
    'Thank you for contacting us. We will respond to you as soon as possible.' => 'Спасибо за обращение! Мы с Вами свяжемся.',
    'There was an error sending email.' => 'Возникла ошибка при отправке письма.',
    'Your Name' => 'Ваше имя',
    'Your e-Mail' => 'Ваш e-Mail',
    'Subject' => 'Тема',
    'Message' => 'Сообщение',
    'Verification Code' => 'Проверочный код',
    'Send' => 'Отправить',
    'Create' => 'Создать',
    'Update' => 'Изменить',
    'Delete' => 'Удалить',
    'Save' => 'Сохранить',
    'Required Field' => 'Обязательное поле',
    'Title' => 'Тайтл',
    'Content' => 'Контент',
    'page {pageNum}' => 'страница {pageNum}',
    // Gallery preview hints
    'Next' => 'Вперёд',
    'Previous' => 'Назад',
    'Last' => 'Последнее',
    'Popular' => 'Популярное',
    'Comments' => 'Коментарии',
    'New Posts' => 'Новые публикации',
    // Error page
    'The requested page does not exist.' => 'Страница не найдена.',
    'The above error occurred while the Web server was processing your request.' => 'Указанная ошибка возникла во время обработки сервером Вашего запроса.',
    'Please contact us if you think this is a server error. Thank you.' => 'Пожалуйста, сообщите нам, если у Вас есть идеи по её устранению. Спасибо.',
    // Tools
    'The &laquo;{type}&raquo; cache was cleared' => 'Кэш &laquo;{type}&raquo; очищен',
    'Re-indexed {count} records' => 'Переиндексировано {count} записей',
    // Site search
    'Site Search' => 'Поиск по сайу',
    'Searched phrase: &laquo;<b>{searchPhrase}</b>&raquo;' => 'Искомая фраза: &laquo;<b>{searchPhrase}</b>&raquo;',
    'Your search results ({count})' => 'Результаты поиска ({count})',
    'In all categories' => 'Во всех категориях',
    'Search...' => 'Поиск...',
    'Sphinx is not enabled. Check configuration file.' => 'Sphinx не включён. Проверьте конфигурационный файл.',
    'The search phrase needs to be longer than 3 but shorter then 100 symbols. Try again please.' => 'Искомая фраза должна быть от 3 до 100 символов. Попробуйте ещё раз, пожалуйста.'
];