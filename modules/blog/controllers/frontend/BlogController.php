<?php
namespace modules\blog\controllers\frontend;

use Yii;
use common\models\LoginForm;
use modules\blog\Module;
use modules\blog\models\frontend\PasswordResetRequestForm;
use modules\blog\models\frontend\ResetPasswordForm;
use modules\blog\models\frontend\SignupForm;
use modules\blog\models\frontend\ContactForm;
use modules\blog\models\frontend\SearchForm;
use modules\blog\models\Post;
use modules\blog\models\Category;
use modules\blog\components\ContentHelper;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Blog controller
 */
class BlogController extends Controller {
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,    // to allow registration change 'allow' to true
                        'roles' => ['?'],  // and uncomment
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                    'footer' => ['post'],
                    'search-bar' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {
        $this->layout = '//main';
        return $this->render('index');
    }

    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', Module::t('blog', 'Thank you for contacting us. We will respond to you as soon as possible.'));
            } else {
                Yii::$app->session->setFlash('error', Module::t('blog', 'There was an error sending email.'));
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout() {
        return $this->render('about');
    }

    public function actionSignup() {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {

                $auth = Yii::$app->authManager;
                $userRole = $auth->getRole('user');
                $auth->assign($userRole, $user->id);

                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionRequestPasswordReset() {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', Module::t('blog', 'Check your email for further instructions.'));

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', Module::t('blog', 'Sorry, we are unable to reset password for email provided.'));
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token) {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', Module::t('blog', 'New password was saved.'));

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionSitemap() {

        $posts = (new \yii\db\Query)
            ->select(
                [
                    'p.id',
                    //'p.date',
                    'p.category_id',
                    'p.category_url_id',
                    'p.alias',
                    'p.name',
                    'c.order',
                    //'c.parent_id',
                    //'p.anons',
                    //'p.images',
                ])
                ->from([
                    Post::tableName().' p',
                    Category::tableName().' c',
                ])
                ->andWhere('p.category_id = c.id')
                ->andWhere(['p.status' => Post::POST_PUBLISHED])
                ->orderBy(['c.order' => SORT_ASC])
                ->all();

        $categories = Category::getCategories();

        foreach ($categories as $id => $cat) {
            $catsCount = 0;
            foreach($categories as $c) {
                if ($c['parent_id'] == $id) {
                    $catsCount++;
                }
            }
            $categories[$id]['catsCount'] = $catsCount;
        }

        return $this->render('sitemap', [
            'posts' => $posts,
            'categories' => $categories
        ]);
    }

    public function actionPrivacy() {
        return $this->render('privacy');
    }

    public function actionFooter() {
        if (!Yii::$app->request->isAjax) {
            throw new BadRequestHttpException('The requested page does not exist.');
        }

        return $this->renderAjax('/blocks/_footer');
    }

    public function actionSearchBar() {
        if (!Yii::$app->request->isAjax) {
            throw new BadRequestHttpException('The requested page does not exist.');
        }

        $categories = Category::getCategories();
        $rootCategories = [];

        foreach ($categories as $c) {
            if ($c['parent_id']) {
                continue;
            }
            $rootCategories[] = [
                'id' => $c['id'],
                'crumb' => $c['crumb']
            ];
        }

        return $this->renderAjax('/blocks/_search_bar', [
            'model' => new SearchForm(),
            'categories' => $rootCategories
        ]);
    }

    public function actionSearch() {
        $model = new SearchForm();
        $model->load(Yii::$app->request->post());

        $search = trim(strip_tags($model->search));
        $message = '';

        if (!$search) {
            $message = Module::t('blog', 'The search phrase needs to be longer than 3 but shorter then 100 symbols. Try again please.');
        }

        if (!Yii::$app->getModule('blog')->getParam('isSphinxEnabled')) {
            $message = Module::t('blog', 'Sphinx is not enabled. Check configuration file.');
        }

        if ($message) {
            Yii::$app->session->setFlash('danger', $message);
            return $this->actionIndex();
        }

        $inCategories = [];

        if ($model->search_category) {
            $inCategories[] = $model->search_category;

            $categories = Category::getCategories();
            $root_id = 0;

            foreach ($categories as $id => $cat) {
                if ($cat['parent_id']) {
                    if ($cat['parent_id'] !== $root_id) {
                        $root_id = $this->findRootSection($cat['id'], $categories);
                    }
                }
                else {
                    $root_id = $cat['id'];
                }

                if ($cat['parent_id'] && $root_id == $model->search_category) {
                    $inCategories[] = $cat['id'];
                }
            }
        }

        $isSphinxActive = false;

        try {
            $query = new \yii\sphinx\Query;
            $isSphinxActive = count($query->connection->getSchema()->indexNames);
        } catch (\Exception $e) {
            // todo:
            // Send email about Sphinx unavailability
        }

        if (!$isSphinxActive) {
            $message = Module::t('blog', 'Sphinx is not active. Inform admin, please.');
            Yii::$app->session->setFlash('danger', $message);
            return $this->actionIndex();
        }

        $search = ContentHelper::prepareKeywords($search, true, 100);

        $options = [
            'before_match' => "<b>",
            'after_match' => "</b>",
            'chunk_separator' => " ... ",
            'html_strip_mode' => "strip",
            //'html_strip_mode' => 'none'
            'passage_boundary' => "none",
            'limit' =>245,
            'limit_passages' => 0,
            'limit_words' => 0,
            'around' => 5,
            'start_passage_id' => 1,
            'exact_phrase' => false,
            'use_boundaries' =>true,
            'weight_order' =>false,
            'query_mode' => false,
            'force_all_words' => false,
            'load_files' =>false,
            'allow_empty' => false,
            'emit_zones' =>false,
        ];

        $query = new \yii\sphinx\Query;

        if (count($inCategories)) {
            //$query->where(['in', 'category_id', $inCategories]);
            $query->where('category_id in('.implode(',', $inCategories).')');
        }

        $sphinxRows = $query->select('*')->from(Yii::$app->getModule('blog')->getParam('sphinxRtIndex'))
            ->match(new \yii\db\Expression(':match',
                [
                    'match' => implode(' | ', $search)
                ]
            ))
            ->snippetCallback(function ($rows) {
                $ids = ArrayHelper::map($rows, 'id', 'id');
                $postModels = Post::find()
                    ->select([
                        'id',
                        'name',
                        'title',
                        'description',
                        'text'
                    ])
                    ->andWhere(['in', 'id', $ids])
                    ->orderBy([new \yii\db\Expression('FIND_IN_SET(id, :id)')])
                    ->addParams([':id' => implode(',', $ids)])
                    ->all();

                foreach ($postModels as $model) {
                    $result[] = /*$model->name . ' '. $model->title . ' ' . $model->description . ' ' .*/
                                $model->text;
                }
                return $result;
            })
            ->snippetOptions($options)
            ->limit(Yii::$app->getModule('blog')->getParam('sphinxSearchLimit'))
            ->all();

        $snippets = ArrayHelper::map($sphinxRows, 'id', 'snippet');
        $models = [];

        if (count($sphinxRows)) {
            $ids = ArrayHelper::map($sphinxRows, 'id', 'id');
            $models = Post::find()
                ->select([
                    'id',
                    'alias',
                    'category_id',
                    'category_url_id',
                    'name',
                    'images',
                ])
                ->andWhere(['in', 'id', $ids])
                ->orderBy([new \yii\db\Expression('FIND_IN_SET(id, :id)')])
                ->addParams([':id' => implode(',', $ids)])
                ->all();
        }

        return $this->render('search', [
            'searchPhrase' => implode(' ', $search),
            'snippets' => $snippets,
            'searchModels' => $models,
            'categories' => Category::getCategories(),
        ]);
    }

    protected function findRootSection($category_id, $categories) {
        while ($parent_id = $categories[$category_id]['parent_id']) {
            $category_id = $parent_id;
        }
        return $category_id;
    }
}
