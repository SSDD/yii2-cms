<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 18.05.2015
 * Time: 9:59
 */
namespace modules\blog\controllers\frontend;
use Yii;
use modules\blog\Module;
use modules\blog\models\Comment;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class CommentController extends Controller {
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'create' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Creates a new Comment model.
     * @return mixed
     */
    public function actionCreate() {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if(Yii::$app->modules['blog']->getParam('blogCommentsStatus') !== Module::BLOG_COMMENTS_ON) {
            return 'Status::'.Yii::$app->modules['blog']->getParam('blogCommentsStatus');
        }

        $model = new Comment(['scenario' => 'backend']);

        if ($model->load(Yii::$app->request->post())) {

            if (Yii::$app->user->isGuest) {
                $model->setAttribute('user_id', 0);
            }
            else {
                $model->setAttribute('user_id', Yii::$app->user->identity->id);
                $model->setAttribute('user_name', Yii::$app->user->identity->username);
                $model->setAttribute('email', Yii::$app->user->identity->email);
            }

            $model->setAttribute('date', Yii::$app->formatter->asDatetime(time(), 'php:Y.m.d H:i:s'));
            $model->setAttribute('status', Comment::PENDING);

            $ip = isset($_SERVER['HTTP_X_REAL_IP']) ? $_SERVER['HTTP_X_REAL_IP'] : (
                isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : ''
            );
            $model->setAttribute('ip', $ip);

            if ($model->validate()) {
                if ($model->save(false)) {

                    $message[] = \modules\blog\Module::t('blog/comment', 'The comment has been added.');
                    if ($model->status == Comment::PENDING) {
                        $message[] = \modules\blog\Module::t('blog/comment', 'Please, wait while moderation is in progress.');
                    }

                    Yii::$app->session->setFlash('success', \modules\blog\Module::t('blog/comment', implode('<br>', $message)));
                    return 'ok';
                }
                else {
                    throw new HttpException(500, 'Error DB Insert record');
                }
            }
            else {
                throw new HttpException(500, 'Error DB Insert record');
            }
        }
    }

}