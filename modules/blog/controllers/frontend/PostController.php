<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 19.03.2015
 * Time: 15:46
 */
namespace modules\blog\controllers\frontend;

use modules\blog\models\Category;
use Yii;
use yii\helpers\HtmlPurifier;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use modules\blog\models\Post;
use modules\blog\models\Comment;
use modules\blog\components\ContentHelper;

class PostController extends Controller {

    public $pageNum = null;

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['get'],
                    'with-category' => ['get'],
                    'post-bar' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex($alias, $pageNum=null) {
        $model = $this->findModelByAlias($alias);

        // Compare requested-alias and db-aliases case
        if ($model->alias !== $alias) {
            $this->redirect(
                Yii::$app->getUrlManager()->createUrl(
                    ContentHelper::getPostUrl($model->category_url_id, $model->id, $model->alias, $pageNum)
                ),
                301
            );
            Yii::$app->end();
        }

        if (in_array($pageNum, array('0', '1'))) {
            $this->redirect(
                Yii::$app->getUrlManager()->createUrl(
                    ContentHelper::getPostUrl($model->category_url_id, $model->id, $model->alias, null)
                ),
                301
            );
            Yii::$app->end();
        }

        $pageNum = intval($pageNum);

        $model->title = HtmlPurifier::process($model->title);
        $model->name = HtmlPurifier::process($model->name);
        //$model->text = HtmlPurifier::process($model->text);

        return $this->render('index', [
            'pageNum' => $pageNum,
            'breadcrumbs' => Yii::$app->categoryTools->breadcrumbs($model->category_id, $model->name),
            'category' => Category::getCategories()[$model->category_id],
            'model' => $model,
        ]);
    }

    public function actionWithCategory($category='', $id, $alias, $pageNum=null) {
        return $this->actionIndex($alias, $pageNum);
    }

    public function actionPostBar() {

        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $tabs = Yii::$app->request->get('tabs', ['all']);
        $tabs = !is_array($tabs) ? [$tabs] : $tabs;

        $viewParams = [];

        if (array_intersect($tabs, ['all', 'last'])) {
            $viewParams['lastPosts'] = Post::lastPosts();
        }

        // Popular Posts
        if (array_intersect($tabs, ['all', 'popular'])) {
            $viewParams['popPosts'] = Post::popularPosts();
        }

        // Comments
        if (array_intersect($tabs, ['all', 'comments'])) {
            $viewParams['lastCommentsArray'] = Comment::lastCommentsArray();
        }

        return $this->renderAjax('/blocks/_post_bar', $viewParams);
    }

    public function actionSocialShare() {
        return $this->renderAjax('/blocks/_social_share');
    }

    protected function findModelByAlias($alias) {
        if (($model = Yii::$app->cache->get(Post::CACHE_POST_MODEL . $alias)) === false) {
            if (($model = Post::find()
                    ->select([
                        Post::tableName().'.*',
                        '(SELECT COUNT(com.post_id)'.
                        ' FROM '.Comment::tableName().' com'.
                        ' WHERE com.post_id = post.id AND com.status="'.Comment::APPROVED.'") as commentCount'
                    ])
                    ->where(['alias' => $alias])
                    ->andWhere(['status' => Post::POST_PUBLISHED])->one()) !== null) {

                //if (($model = Post::findOne(['alias' => $alias, 'status' => Post::POST_PUBLISHED])) !== null) {
                Yii::$app->cache->set(Post::CACHE_POST_MODEL . $alias, $model, $this->module->getParam('cacheDuration'));
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        return $model;
    }

}