<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 19.03.2015
 * Time: 15:46
 */
namespace modules\blog\controllers\frontend;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\HtmlPurifier;
use yii\data\Pagination;
use modules\blog\models\Category;
use modules\blog\models\Post;
use modules\blog\models\Comment;

class CategoryController extends Controller {

    public function actionIndex($alias, $pageNum=null) {

        if (in_array($pageNum, array('0', '1'))) {
            $this->redirect(
                Yii::$app->getUrlManager()->createUrl(
                    [
                        'blog/category/index',
                        'alias' => $alias
                    ]
                ),
                301
            );
            Yii::$app->end();
        }

        $pageNum = intval($pageNum);

        $model = $this->findModelByAlias($alias);

        // Compare alias case
        if ($model->alias !== $alias) {
            $this->redirect(
                Yii::$app->getUrlManager()->createUrl(
                    [
                        'blog/category/index',
                        'alias' => $model->alias,
                        'pageNum' => $pageNum
                    ]
                ),
                301
            );
            Yii::$app->end();
        }

        $model->name = HtmlPurifier::process($model->name);
        $model->crumb = HtmlPurifier::process($model->crumb);
        $model->title = HtmlPurifier::process($model->title);
        $model->snippet = HtmlPurifier::process($model->snippet);
        $model->text = HtmlPurifier::process($model->text);

        $query = Post::find()
            ->select([
                Post::tableName().'.*',
                '(SELECT COUNT(com.post_id)'.
                ' FROM '.Comment::tableName().' com'.
                ' WHERE com.post_id = post.id AND com.status="'.Comment::APPROVED.'") as commentCount'
            ])
            ->andWhere(['category_id' => $model->id, 'status' => Post::POST_PUBLISHED])->orderBy(['post.date' => SORT_DESC]);

        $countQuery = clone $query;
        $totalCount = $countQuery->count();
        $postsPerPage = Yii::$app->getModule('blog')->getParam('postsPerPage');

        $pagesCount = ceil($totalCount / $postsPerPage);

        if ($pageNum > $pagesCount) {
            $this->redirect(
                Yii::$app->getUrlManager()->createUrl(
                    [
                        'blog/category/index',
                        'alias' => $alias
                    ]
                ),
                301
            );
            Yii::$app->end();
        }

        $pages = new Pagination([
            'totalCount' => $totalCount,
            'pageSize' => $postsPerPage,
            'defaultPageSize' => $postsPerPage,
            'pageParam' => 'pageNum',
            'forcePageParam' => false,
        ]);
        $postModels = $query->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('index', [
            'subCategories' => Yii::$app->categoryTools->subCategoryIds($model->id),
            'pageNum' => $pageNum,
            'breadcrumbs' => Yii::$app->categoryTools->breadcrumbs($model->id, $model->crumb, $model->id),
            'model' => $model,
            'postModels' => $postModels,
            'pages' => $pages,
        ]);
    }

    protected function findModelByAlias($alias) {

        if (($model = Yii::$app->cache->get(Category::CACHE_CATEGORY_MODEL . $alias)) === false) {
            if (($model = Category::findOne(['alias' => $alias])) !== null) {
                Yii::$app->cache->set(Category::CACHE_CATEGORY_MODEL . $alias, $model, $this->module->getParam('cacheDuration'));
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }

        return $model;
    }
}