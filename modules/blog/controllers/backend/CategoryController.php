<?php

namespace modules\blog\controllers\backend;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use modules\blog\models\Category;
use modules\blog\models\backend\CategorySearch;
use himiklab\sortablegrid\SortableGridAction;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'logout', 'sort'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'sort' => ['post'],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Category::className()
            ]
        ];
    }
    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex() {

        if (!\Yii::$app->user->can('accessCategories')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider->sort->defaultOrder = ['order' => SORT_ASC];
        $dataProvider->sort = false;
        $dataProvider->pagination = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categories' => Category::getCategories(),
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {

        if (!\Yii::$app->user->can('createCategory')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = new Category();

        if ( $model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $this->logo($model);
                Category::clearCache();
                return $this->redirect(['index']);
            }
            else {
                throw new HttpException(500, 'Error DB Insert record');
            }

        } else {
            $categoriesForDropDown = Yii::$app->categoryTools->categoriesForDropDown();

            return $this->render('create', [
                'categoriesArray' => $categoriesForDropDown['array'],
                'categoriesOptions' => $categoriesForDropDown['options'],
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {

        if (!\Yii::$app->user->can('updateCategory')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = $this->findModel($id);
        $isLoad = $model->load(Yii::$app->request->post());
        $isValid = $isLoad ? $model->validate() : false;

        $oldParent = $isValid ? $model->getOldAttribute('parent_id') : null;

        if ($isValid && $model->save(false)) {
            $this->logo($model);
            Category::clearCache($model->alias);

            return $this->redirect(['index']);
        } else {
            $categoriesForDropDown = Yii::$app->categoryTools->categoriesForDropDown();
            unset($categoriesForDropDown['array'][$model->id]);
            unset($categoriesForDropDown['options'][$model->id]);

            return $this->render('update', [
                'categoriesArray' => $categoriesForDropDown['array'],
                'categoriesOptions' => $categoriesForDropDown['options'],
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {

        if (!\Yii::$app->user->can('deleteCategory')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = $this->findModel($id);
        Category::clearCache($model->alias);
        $model->delete();
        return $this->redirect(['index']);
    }

    protected function logo($model) {
        if (!$model->imageRemove) {

            // Get the instance of the uploaded file
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if (!is_null($model->imageFile)) {
                $imageName = $model->id . '.' . $model->imageFile->extension;
                $model->imageFile->saveAs(Yii::getAlias('@images') . '/category/' . $imageName);
                $model->image = $imageName;
                $model->save(false);
            }
        }
        else {
            if ($model->image) {
                if (file_exists(Yii::getAlias('@images').'/category/'.$model->image)) {
                    @unlink(Yii::getAlias('@images').'/category/'.$model->image);
                }

                $model->image = '';
                $model->save(false);
            }

        }
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
