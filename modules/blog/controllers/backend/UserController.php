<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 22.05.2015
 * Time: 22:36
 */
namespace modules\blog\controllers\backend;
use common\models\User;

class UserController extends \modules\blog\controllers\UserController {

    /**
     * This method selects username for Kartik's Select2 widget. It's set up in comment's form for choosing users.
     * @param null $q
     * @param null $id
     * @return array
     */
    public function actionUserList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '', 'email' => '']];
        if (!is_null($q)) {
            $query = new \yii\db\Query;
            $query->select('id, username AS text, email')
                ->from('user')
                ->where('username LIKE "%' . $q .'%"')
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {die;
            $user = User::find($id);
            //print_r($user->attributes);
            $out['results'] = ['id' => $id, 'text' => $user->email/*, 'email' => $user->email*/];
        }
        return $out;
    }

}