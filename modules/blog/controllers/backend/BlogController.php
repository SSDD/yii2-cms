<?php
namespace modules\blog\controllers\backend;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use common\models\LoginForm;
use modules\blog\models\Category;
use modules\blog\models\Post;
use modules\blog\models\Comment;

/**
 * Site controller
 */
class BlogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'clear-cache', 'reindex'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout = '//main';
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionClearCache($type) {

        $categories = null;
        if (in_array($type, ['categories'])) {
            $categories = Category::find()->select('alias')->all();
        }

        $posts = null;
        if (in_array($type, ['posts', 'related'])) {
            $posts = Post::find()->select(['id', 'alias'])->all();
        }

        switch ($type) {
            case 'all': {
                Yii::$app->cache->flush();
                break;
            }
            case 'categories': {
                Yii::$app->cache->delete(Category::CACHE_CATEGORIES_ARRAY);
                foreach ($categories as $category) {
                    Yii::$app->cache->delete(Category::CACHE_CATEGORY_MODEL.$category->getAttribute('alias'));
                }
                break;
            }
            case 'posts': {
                foreach ($posts as $post) {
                    Yii::$app->cache->delete(Post::CACHE_POST_MODEL.$post->getAttribute('alias'));
                    Comment::clearCache($post->getAttribute('id'));
                }
                break;
            }
            case 'related': {
                foreach ($posts as $post) {
                    Yii::$app->cache->delete(Post::CACHE_REL_POSTS.$post->getAttribute('alias').'_0');
                    Yii::$app->cache->delete(Post::CACHE_REL_POSTS.$post->getAttribute('alias').'_1');
                }
                break;
            }

            case 'popular': {
                Yii::$app->cache->delete(Post::CACHE_POP_POSTS);
                break;
            }
            case 'last': {
                Yii::$app->cache->delete(Post::CACHE_POP_POSTS);
                break;
            }
            case 'comments': {
                Yii::$app->cache->delete(Comment::CACHE_LAST_COMMENTS);
                break;
            }
            default : {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }

        $message = \modules\blog\Module::t('blog', 'The &laquo;{type}&raquo; cache was cleared', ['type' => $type]);
        Yii::$app->session->setFlash('success', $message);

        $this->redirect(['blog/index'], 301);
        Yii::$app->end();
    }

    public function actionReindex() {

        if (Yii::$app->getModule('blog')->getParam('isSphinxEnabled')) {
            $postsIds = Post::find()->select('id')->all();

            foreach ($postsIds as $postId) {

                $post = Post::find()
                    ->select([
                        'id',
                        'category_id',
                        'name',
                        'description',
                        'text'
                    ])
                    ->where('id = :id', [':id' => $postId->id])->one();

                $params = [];

                $sql = \Yii::$app->sphinx->getQueryBuilder()
                    ->replace(
                        Yii::$app->getModule('blog')->getParam('sphinxRtIndex'),
                        [
                            'id' => $post->id,
                            'name' => $post->name,
                            'description' => $post->description,
                            'text' => $post->text,
                            'category_id' => strval($post->category_id)
                        ],
                        $params
                    );
                Yii::$app->sphinx->createCommand($sql, $params)->execute();
            }

            $message = \modules\blog\Module::t('blog', 'Re-indexed {count} records', ['count' => count($postsIds)]);
        }
        else {
            $message = 'You need to enable Sphinx in the config.';
        }

        Yii::$app->session->setFlash('success', $message);

        return $this->render('index');
    }
}
