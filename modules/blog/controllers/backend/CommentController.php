<?php

namespace modules\blog\controllers\backend;
use Yii;
use modules\blog\models\Comment;
use modules\blog\models\backend\CommentSearch;
use modules\blog\models\Post;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * CommentController implements the CRUD actions for Comment model.
 */
class CommentController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Comment models.
     * @return mixed
     */
    public function actionIndex() {

        if (!\Yii::$app->user->can('accessComments')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $searchModel = new CommentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['date' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'statusArray' => Comment::getStatusArray(),
        ]);
    }

    /**
     * Displays a single Comment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {

        if (!\Yii::$app->user->can('accessComments')) {
            throw new ForbiddenHttpException('Access denied');
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Comment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {

        if (!\Yii::$app->user->can('createComment')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = new Comment(['scenario' => 'backend']);

        if ($model->load(Yii::$app->request->post())) {
            if (!$model->setUserById()) {
                throw new HttpException(500, 'Error DB Insert record');
            }

            if ($model->save()) {
                Comment::clearCache($model->post_id);
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                throw new HttpException(500, 'Error DB Insert record');
            }

        } else {

            $model->setAttribute('date', Yii::$app->formatter->asDatetime(time(), 'php:Y.m.d H:i:s'));
            $postUrl = \yii\helpers\Url::to(['post/post-list']);
            $postName = '';
            $userUrl = \yii\helpers\Url::to(['user/user-list']);
            $userName = '';

            return $this->render('create', [
                'model' => $model,
                'statusArray' => Comment::getStatusArray(),
                'postUrl' => $postUrl,
                'postName' => $postName,
                'userUrl' => $userUrl,
                'userName' => $userName,
            ]);
        }
    }

    /**
     * Updates an existing Comment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {

        if (!\Yii::$app->user->can('updateComment')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if (!$model->setUserById($model)) {
                throw new HttpException(500, 'Error DB Update record');
            }

            if ($model->save()) {
                Comment::clearCache($model->post_id);
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else {
                throw new HttpException(500, 'Error DB Update record');
            }

        } else {

            $postUrl = \yii\helpers\Url::to(['post/post-list']);
            $postName = Post::findOne($model->post_id)->name;
            $userUrl = \yii\helpers\Url::to(['user/user-list']);

            $userName = !$model->user_id ? '' : \common\models\User::findIdentity($model->user_id)->username;

            return $this->render('update', [
                'model' => $model,
                'statusArray' => Comment::getStatusArray(),
                'postUrl' => $postUrl,
                'postName' => $postName,
                'userUrl' => $userUrl,
                'userName' => $userName,
            ]);
        }
    }

    /**
     * Deletes an existing Comment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {

        if (!\Yii::$app->user->can('deleteComment')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = $this->findModel($id);
        Comment::clearCache($model->post_id);
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Comment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Comment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Comment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
