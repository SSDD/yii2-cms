<?php

namespace modules\blog\controllers\backend;

use Yii;

use modules\blog\models\Post;
use modules\blog\models\backend\PostSearch;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use vova07\imperavi\actions\GetAction;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    //'post-list' => ['get'],
                ],
            ],
            'httpCache' => [
                'class' => 'yii\filters\HttpCache',
                'only' => ['index'],
                'lastModified' => function() {
                    return 0;
                },
                'sessionCacheLimiter' => 'nocache',
                'cacheControlHeader' => 'max-age=0, proxy-revalidate, must-revalidate'
            ],
        ];
    }

    public function actions()
    {
        $id = Yii::$app->request->get('id');
        $onlyImages = ($id ? $id . '-' : '') .'*.*';

        $imgSubPath = Yii::$app->request->get('path', '');

        return [
            'images-get' => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url' => Yii::$app->getModule('blog')->getParam('imagesBaseUrl').$imgSubPath, // URL адрес папки где хранятся изображения.
                'path' => '@images'.($imgSubPath ? '/'.$imgSubPath : ''), // Или абсолютный путь к папке с изображениями.
                'options' => ['recursive' => false, 'only' => [$onlyImages]],
                'type' => GetAction::TYPE_IMAGES,
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex() {

        if (!\Yii::$app->user->can('accessPosts')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];
        $categoriesForDropDown = Yii::$app->categoryTools->categoriesForDropDown();
        //echo $this->getViewPath();die;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categoriesArray' => $categoriesForDropDown['array'],
            'categoriesOptions' => $categoriesForDropDown['options'],
            'statusArray' => Post::getStatusArray(),
            'commentsStateArray' => Post::getCommentsStateArray(),
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {

        if (!\Yii::$app->user->can('createPost')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = new Post();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->clearCache($model->alias);
            return $this->redirect(['index']);
        } else {
            $categoriesForDropDown = Yii::$app->categoryTools->categoriesForDropDown();
            $model->setAttribute('date', Yii::$app->formatter->asDatetime(time(), 'php:Y.m.d H:i:s'));

            return $this->render('create', [
                'model' => $model,
                'user_id' => Yii::$app->user->identity->id,
                'user_name' => Yii::$app->user->identity->username,
                'categoriesArray' => $categoriesForDropDown['array'],
                'categoriesOptions' => $categoriesForDropDown['options'],
                'statusArray' => Post::getStatusArray(),
                'commentsStateArray' => Post::getCommentsStateArray(),
            ]);
        }
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {

        $this->enableCsrfValidation = false;
        Yii::$app->request->enableCsrfValidation = false;

        $model = $this->findModel($id);

        if (!\Yii::$app->user->can('updatePost', ['post' => $model])) {
            throw new ForbiddenHttpException('Access denied');
        }

        $oldAlias = $model->alias;

        // Y-m-d h:i:s
        //$dateArray = date_parse_from_format('Y-m-d H:i:s', $model->attributes['date']);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->clearCache($oldAlias);
            return $this->redirect(['index']);
        }
        else {
            $categoriesForDropDown = Yii::$app->categoryTools->categoriesForDropDown();

            return $this->render('update', [
                'model' => $model,
                'user_id' => $model->user_id,
                'user_name' => $model->getUserName(),
                'categoriesArray' => $categoriesForDropDown['array'],
                'categoriesOptions' => $categoriesForDropDown['options'],
                'statusArray' => Post::getStatusArray(),
                'commentsStateArray' => Post::getCommentsStateArray(),
            ]);
        }
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {

        if (!\Yii::$app->user->can('deletePost')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = $this->findModel($id);
        $this->clearCache($model->alias);
        $model->delete();
        return $this->redirect(['index']);
    }

    public function actionCropMe($id=null) {
        $model = $id ? $this->findModel($id) : (new Post());
        $model->setScenario('cropMe');

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $model->image = \yii\web\UploadedFile::getInstance($model, 'image');

            if ($model->save()) {
                // to do
                // flash message
            }
            Yii::$app->end();
        }

        return $this->renderAjax('cropMe', [
            'model' => $model,
        ]);
    }

    public function actionImageRemove() {
        if (!Yii::$app->request->isAjax) {
            echo 'Not Ajax request';
            Yii::$app->end();
        }

        $path = rtrim(Yii::getAlias('@images'), DIRECTORY_SEPARATOR);

        $post_id = Yii::$app->request->get('id');
        $imageName = Yii::$app->request->get('imageName');

        $files = [
            $path . '/'. $imageName,
            $path . '/mini/' . $imageName,
            $path . '/thumb/' . $imageName,
            $path . '/full/' . $imageName
        ];

        $removed = 0;

        foreach($files as $file) {
            if (file_exists($file) && unlink($file)) {
                $removed++;
            }
        }

        $model = $this->findModel($post_id);
        $postImages = unserialize($model->images);

        if (($pos = array_search($imageName, $postImages)) !== false) {
            unset($postImages[$pos]);

            $images = Post::serializeImages($postImages);

            if (count($postImages)) {
                rsort($postImages);
                $postImages = serialize($postImages);
            }
            else {
                $postImages = '';
            }
            $model->setAttribute('images', $images);
            $model->update(false, ['images']);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['removed' => $removed];
    }

    /**
     * This method selects posts for Kartik's Select2 widget. It's set up in comment form for choosing post.
     * @param null $q
     * @param null $id
     * @return array
     */
    public function actionPostList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new \yii\db\Query;
            $query->select('id, name AS text')
                ->from('post')
                ->where('name LIKE "%' . $q .'%"')
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Post::find($id)->name];
        }
        return $out;
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    protected function clearCache($alias) {
        Post::clearCache($alias);
        \modules\blog\models\Category::clearCache(null);
    }
}
