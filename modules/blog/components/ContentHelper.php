<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 11.05.2015
 * Time: 10:31
 */
namespace modules\blog\components;
use Yii;
use yii\helpers\Url;

class ContentHelper {

    public static function anons($text, $beg_pos=0, $len=200, $dots=true) {
        $anons = '';

        $enc = mb_internal_encoding();
        mb_internal_encoding(Yii::$app->charset);

        $text = self::clean($text);

        $text_len = mb_strlen($text);

        if ($beg_pos > $text_len) {
            $beg_pos = 0;
        }

        if (($beg_pos+$len) > $text_len) {
            $len = $text_len-$beg_pos;
        }

        // if not from the beginning
        if ($beg_pos) {
            $beg_pos = mb_strpos($text, '.', $beg_pos);

            if ( ! is_numeric($beg_pos)) {
                $beg_pos = 0;
            }
            else {
                $beg_pos++;
            }
        }

        // seek first space after the limit mark
        $end_pos = mb_strpos($text, ' ', $beg_pos+$len);
        if (!is_numeric($end_pos)) {
            $end_pos = mb_strlen($text);
        }

        $anons = trim(mb_substr($text, $beg_pos, $end_pos-$beg_pos));

        if ($dots) {
            if (mb_strlen($text) > mb_strlen($anons)) {
                if ($beg_pos) {
                    $anons = '... '.$anons;
                }
                $anons .= ' ...';
            }
        }

        $anons = strip_tags($anons);
        mb_internal_encoding($enc);

        return $anons;
    }

    public static function insertAd($text) {
        $a_ad_tag = $a_ad = [];
        require(Yii::getAlias('@frontend').'/config/ad.php');
        return str_replace($a_ad_tag, $a_ad, $text);
    }

    public static function clean($text) {
        $text = html_entity_decode($text, ENT_QUOTES, Yii::$app->charset);
        $text = strip_tags($text);
        $text = str_replace([
            "`","~","!","@","#","\"","№",";","$","%","^",":","&","?",
            "*","(",")","-","_","+","=","<",">","/","|","\\",//".",",",
            "'","[","]","{","}","«","»","•"
        ]," ",$text);

        return trim(preg_replace('/\s\s+/', ' ', $text));
    }

    public static function getImageSrc($subDir = '', $imagesSerialized) {
        $subDir = $subDir ? $subDir . '/' : '';
        $images = unserialize($imagesSerialized);
        $image = '';

        if (count($images)) {
            $image = Yii::$app->getModule('blog')->getParam('imagesBaseUrl').$subDir.$images[0];
        }

        return $image;
    }

    /**
     * Get either a Gravatar URL or complete image tag for a specified email address.
     *
     * @param string $email The email address
     * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
     * @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
     * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
     * @param boole $img True to return a complete IMG tag False for just the URL
     * @param array $atts Optional, additional key/value attributes to include in the IMG tag
     * @return String containing either just a URL or a complete image tag
     * @source http://gravatar.com/site/implement/images/php/
     */
    public static function getGravatar($email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array()) {
        $url = 'http://www.gravatar.com/avatar/';
        $url .= md5( strtolower( trim( $email ) ) );
        $d = urlencode($d);
        $url .= "?s=$s&d=$d&r=$r";
        if ( $img ) {
            $url = '<img src="' . $url . '"';
            foreach ( $atts as $key => $val )
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }
        return $url;
    }

    public static function getPostUrl($category_url_id, $id, $alias, $pageNum=null, $isBackEnd=false) {
        $params = [];

        if (Yii::$app->modules['blog']->getParam('categoryInUrl') && intval($category_url_id)) {
            $params = [
                ($isBackEnd?'blog/':'').'post/with-category',
                'category' => \modules\blog\models\Category::getCategories()[intval($category_url_id)]['alias'],
                'id' => $id,
                'alias' => $alias
            ];
        }
        else {
            $params = [
                ($isBackEnd?'blog/':'').'post/index',
                'alias' => $alias
            ];
        }

        if ($pageNum) {
            $params['pageNum'] = $pageNum;
        }

        return $isBackEnd ? Yii::$app->urlManagerFrontend->createUrl($params) : Url::to($params);
    }

    public static function prepareKeywords($text, $escape = false, $keywordsCount = 20) {
        $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
        // insert spaces where an opening tag directly follows a closing tag
        $text = preg_replace('/(<\/[^>]+?>)(<[^>\/][^>]*?>)/', '$1 $2', $text);
        $text = strip_tags($text);
        $text = preg_replace("/[^[:alnum:][:space:]]/ui", ' ', $text);
        $text = preg_replace('/\s\s\r\n+/', ' ', $text);

        $array = explode(' ', $text);
        $array = array_count_values($array);
        arsort($array);

        $text = [];

        foreach($array as $word => $count) {
            if (mb_strlen($word, Yii::$app->charset) < 3) {
                continue;
            }

            $text[] = $escape ? Yii::$app->sphinx->escapeMatchValue($word) : $word;

            if (count($text) > $keywordsCount) {
                break;
            }
        }

        return $text;
    }
}