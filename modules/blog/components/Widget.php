<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 24.06.2015
 * Time: 13:09
 */
namespace modules\blog\components;

use Yii;
use yii\base\InvalidConfigException;

/**
 * Base module.
 */
class Widget extends \yii\base\Widget {
    /**
     * @var string|null Widget name
     */
    public $name = null;

    /**
     * @inheritdoc
     */
    public function init() {
        if ($this->name === null) {
            throw new InvalidConfigException('The "name" property must be set.');
        }
        parent::init();
    }

    public function getViewPath() {
        return '@root/modules/blog/widgets/'.$this->name.'/views';
    }
}