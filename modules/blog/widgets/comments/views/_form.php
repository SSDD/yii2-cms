<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 15.05.2015
 * Time: 18:18
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use modules\blog\Module;
use yii\widgets\Pjax;

Pjax::begin(['id' => 'comment-message']);
foreach(Yii::$app->session->getAllFlashes() as $class => $message): ?>
<br>
        <?= \yii\bootstrap\Alert::widget([
                'options' => [
                    'class' => 'col-sm-10 col-sm-offset-1 text-center alert-'. $class,
                ],
                'body' => $message,
            ]);
        ?>
<?php
endforeach;
Pjax::end();
?>

<div class="comment-form">
    <div class="row">
        <h3 class="col-sm-offset-1 col-sm-10"><?= Module::t('blog/comment', 'Leave a Comment'); ?></h3>
    </div>
    <?php
        $form = ActiveForm::begin([
            'id' => $model->formName(),
            //'method' => 'post',
            //'action' => '/add-comment',
            'options' =>['class' => 'form']
        ]);
    ?>

    <?php if (Yii::$app->user->isGuest): ?>
        <div class="row">
            <div class="col-sm-offset-1 col-sm-5">
                <?= $form->field($model, 'user_name', [
                        'inputOptions' => [
                            'class' => 'form-control',
                            'placeholder' => Module::t('blog/comment', 'Enter your name...'),
                            'required' => 'required',
                        ]
                        //'placeholder' => Module::t('blog/comment', 'Enter your name...')
                    ])->label(false);
                ?>
            </div>
            <div class="col-sm-5">
                <?= $form->field($model, 'email', [
                    'inputOptions' => [
                        'class' => 'form-control',
                        'placeholder' => Module::t('blog/comment', 'Enter your email...')
                    ]
                ])->label(false);
                ?>

            </div>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-offset-1 col-sm-10">
            <?= $form->field($model, 'comment', [
                    'inputOptions' => [
                        'class' => 'form-control',
                        'placeholder' => Module::t('blog/comment', 'Comment text...')
                    ]
                ])->label(false)->textarea();
            ?>
        </div>
    </div>

    <?= Html::activeHiddenInput($model, 'post_id') ?>

    <div class="form-group">
        <div class="col-sm-12 text-center">
            <?= Html::submitButton(Module::t('blog/comment', 'Send comment'), ['class' => 'btn btn-default btn-lg text-center']); ?>
        </div>
    </div>

    <?php ActiveForm::end();

        $js = <<< JS
            $('form#{$model->formName()}').on('beforeSubmit', function(e) {
                var \$form = $(this);
                $.post('/add-comment',
                    \$form.serialize()
                )
                .done(function(result) {
                    if (result == 'ok') {
                        $(\$form).trigger('reset');
                        $.pjax.reload({container:"#comment-message"});
                    }
                    else {
                        //console.log(result);
                        $('#message').html(result);
                    }
                })
                .fail(function() {
                    console.log('server error');
                });
                return false;
            });
JS;

        $this->registerJs($js);

    ?>
</div>