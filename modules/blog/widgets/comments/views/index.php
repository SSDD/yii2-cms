<?php
use yii\helpers\Html;
use justinvoelker\separatedpager\LinkPager;
use modules\blog\Module;
use modules\blog\models\Post;

// comments list begin
if ($commentCount):
?>
<div id="comments" class="comments-list">

    <h4>
        <?= Module::t('blog/comment', 'Comments'); ?>
    </h4>

    <?php foreach($commentModels as $comment): ?>
        <div class="well" id="c<?php echo $comment->comment_id; ?>">

            <div class="media-heading">
                <?= Html::tag('a', '#'.$comment->comment_id, [
                    'href' => '#'.$comment->comment_id,
                    'name' => $comment->comment_id
                ]);
                ?>
                <b><?php echo $comment->user_name; ?></b>

                <div class="pull-right">
                    <small>
                        <?= Module::t('blog/comment', 'dated {date}', ['date' => Yii::$app->formatter->asDate($comment->date)]) . ' ' .
                            Module::t('blog/comment', 'in {time}', ['time' => Yii::$app->formatter->asTime($comment->date)]);
                        ?>
                    </small>
                </div>
            </div>

            <div class="content">
                <?php echo nl2br(Html::encode($comment->comment)); ?>
            </div>
        </div>
    <?php
        endforeach;

        // display pagination
        echo LinkPager::widget([
            'options' => ['class' => 'pagination pagination-sm'],
            'pagination' => $pages,
            'maxButtonCount' => 7,
            'nextPageLabel' => false,
            'prevPageLabel' => false,
            'activePageAsLink' => false,
        ]);
    ?>
</div>
<?php
// end of comments-list
endif;

// New comment form
if (Yii::$app->modules['blog']->getParam('blogCommentsStatus') == Module::BLOG_COMMENTS_ON &&
    $comments_state == Post::COMMENTS_ON
) {
    echo $this->render('_form', ['model' => $model]);
}
?>