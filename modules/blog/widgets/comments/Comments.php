<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 17.05.2015
 * Time: 6:38
 */
namespace modules\blog\widgets\Comments;

use Yii;
use yii\base\InvalidConfigException;
use yii\data\Pagination;
use modules\blog\components\Widget;
use modules\blog\models\Comment;

class Comments extends Widget {

	public $name = 'comments';

    public $post_id;

    public $comments_state;

    public $pageNum;

    public $redirectUrlParams;

    public $commentCount;

    public function init() {
        parent::init();

        if ($this->post_id === null) {
            throw new InvalidConfigException('The "post_id" property must be set.');
        }
    }

    public function run() {
        $query = Comment::find()->andWhere(['post_id' => $this->post_id, 'status' => Comment::APPROVED])->orderBy(['id' => SORT_DESC]);

        if ($this->commentCount == null) {
            $countQuery = clone $query;
            $this->commentCount = $countQuery->count();
        }

        $commentsPerPage = Yii::$app->getModule('blog')->getParam('commentsPerPage');

        $pagesCount = ceil($this->commentCount / $commentsPerPage);

        if ($this->pageNum > $pagesCount && is_array($this->redirectUrlParams)) {
            Yii::$app->response->redirect(
                Yii::$app->getUrlManager()->createUrl($this->redirectUrlParams, 301)
            );
            Yii::$app->end();
        }

        $pages = new Pagination([
            'totalCount' => $this->commentCount,
            'pageSize' => $commentsPerPage,
            'defaultPageSize' => $commentsPerPage,
            'pageParam' => 'pageNum',
            'forcePageParam' => false,
        ]);

        $commentModels = $this->getModels($this->post_id, $this->pageNum, $query, $pages);

        $model = new Comment(['scenario' => 'frontend']);
        $model->setAttribute('post_id', $this->post_id);

        return $this->render('index', [
            'model' => $model,
            'comments_state' => $this->comments_state,
            'commentCount' => $this->commentCount,
            'commentModels' => $commentModels,
            'pages' => $pages,
            'pageNum' => $this->pageNum,
        ]);
    }

    protected function getModels($post_id, $pageNum, $query, $pages) {
        $cachePageNum = !$pageNum ? 1 : $pageNum;

        if (($models = Yii::$app->cache->get(Comment::CACHE_POST_COMMENTS . $post_id .'_' . $cachePageNum)) === false) {
            if ($models = $query->offset($pages->offset)->limit($pages->limit)->all()) {
                Yii::$app
                    ->cache
                    ->set(Comment::CACHE_POST_COMMENTS . $post_id .'_' . $cachePageNum,
                        $models, Yii::$app->getModule('blog')->getParam('cacheDuration'));
            }
        }

        return $models;
    }

}