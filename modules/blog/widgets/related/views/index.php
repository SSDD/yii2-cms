<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 04.06.2015
 * Time: 14:01
 */

?>

<div class="related-posts">
    <?php if ($blockName): ?>
    <h4><?= $blockName; ?></h4>
    <?php endif; ?>

    <div class="list-group">
    <?php foreach ($posts as $post): ?>
        <?= $this->render('_post', ['post' => $post]); ?>
    <?php endforeach; ?>
    </div>
</div>
