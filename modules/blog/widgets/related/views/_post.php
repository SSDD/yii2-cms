<?php
use yii\helpers\Html;
use modules\blog\components\ContentHelper;
?>
<div>
    <?= Html::a($post->name, ContentHelper::getPostUrl($post->category_url_id, $post->id, $post->alias), ['class' => 'list-group-item']); ?>
</div>

