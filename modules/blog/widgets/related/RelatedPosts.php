<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 04.06.2015
 * Time: 14:02
 */

namespace modules\blog\widgets\related;

use Yii;
use yii\base\InvalidConfigException;
use yii\sphinx\Query;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use modules\blog\components\Widget;
use modules\blog\models\Post;

class RelatedPosts extends Widget {
	public $name = 'related';

    public $blockName = '';

    public $postModel;

    public $inCategory = true;

    public $limit = 3;

    public function init() {
        parent::init();

        if ($this->postModel === null) {
            throw new InvalidConfigException('The "postModel" property must be set.');
        }

    }

    public function run() {
        $posts = $this->getPosts();

        if (!count($posts)) {
            return '';
        }

        return $this->render('index', [
            'blockName' => $this->blockName,
            'posts' => $posts
        ]);
    }

    protected function getPosts() {
        $posts = [];
        $isSphinxActive = false;

        try {
            $query = new Query;
            $isSphinxActive = count($query->connection->getSchema()->indexNames);
        } catch (\Exception $e) {
            // todo:
            // Send email about Sphinx unavailability
        }

        $cacheKey = Post::CACHE_REL_POSTS . $this->postModel->alias . '_' . ($this->inCategory ? '1': '0');

        if ((($posts = Yii::$app->cache->get($cacheKey)) === false) && $isSphinxActive) {

            $search = \modules\blog\components\ContentHelper::prepareKeywords(
                $this->postModel->name.' '.
                $this->postModel->title.' '.
                $this->postModel->description.' '.
                $this->postModel->text,
                true, 20
            );

            $postIds = $query
                ->select('id')
                ->from(Yii::$app->getModule('blog')->getParam('sphinxRtIndex'))
                ->match(new Expression(':match',
                    [
                        'match' => implode(' | ', $search)
                    ]
                ))
                ->andWhere('id != :id', [':id' => $this->postModel->id])
                ->andWhere('category_id'.($this->inCategory?'=':'!=').':category_id', [':category_id' => $this->postModel->category_id])
                ->limit($this->limit)
                ->all();

            if (count($postIds)){
                $postIds = ArrayHelper::map($postIds, 'id', 'id');

                $posts = Post::find()
                    ->select(['id', 'category_url_id', 'name', 'alias', 'images'])
                    ->where(['id'=>$postIds])
                    ->andWhere('status=:status', [':status' => Post::POST_PUBLISHED])
                    ->all();
            }
            $posts = !$posts ? [] : $posts;

            Yii::$app->cache->set($cacheKey, $posts, Yii::$app->getModule('blog')->getParam('cacheDuration'));
        }

        return !$posts ? [] : $posts;
    }

}