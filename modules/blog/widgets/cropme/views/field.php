<?php
use yii\helpers\Html;
use modules\blog\widgets\cropme\CropMe;
use kartik\slider\Slider;
use kartik\icons\Icon;

$cropModel = new modules\blog\models\Post(['scenario' => 'cropMe']);

Icon::map($this, Icon::FA);
?>
<div id="<?= $idWidget; ?>" class="cropbox">
    <div class="imageBox">
        <div class="thumbBox"></div>
    </div>

    <div class="cropped">
        <?php
        if (is_string($originalUrl) && !empty($originalUrl))
        {
            echo Html::a(Icon::show('eye') . CropMe::t('Show original'), $originalUrl, [
                'target' => '_blank',
                'class' => 'btn btn-info',
            ]);
        }
        if (!empty($previewUrl))
        {
            foreach ($previewUrl as $url) {
                echo Html::img($url, ['class' => 'img-thumbnail']);
            }
        }
        ?>
    </div>

    <p id="message"></p>

    <div class="btn-group" style="">
        <span class="btn btn-primary btn-file">
            <?= Icon::show('folder-open')
                    .CropMe::t('Browse')
                    .Html::activeFileInput($cropModel, $attribute, $options);
                    //.Html::activeInput('file', $model, $attribute, $options);
            ?>
        </span>
        <?php
        echo Html::button(Icon::show('expand '), [
            'class' => 'btn btn-default btnZoomIn',
        ]);
        echo Html::button(Icon::show('compress'), [
            'class' => 'btn btn-default btnZoomOut',
        ]);
        echo Html::button(Icon::show('crop') . CropMe::t('Crop'), [
            'class' => 'btn btn-success btnCrop',
        ]);
        echo Html::submitButton(Icon::show('upload') . CropMe::t('Upload'), [
            'class' => 'btn btn-primary btnUpload',
        ]);
        echo Html::button(Icon::show('remove') . CropMe::t('Remove'), [
            'class' => 'btn btn-default btnRemove',
        ]);
        ?>
    </div>
    <div class="form-horizontal">
        <div class="form-group resizeWidth">
            <label for="<?= $idWidget; ?>_cbox_resize_width" class="col-md-3"><?= CropMe::t('Width'); ?></label>
            <div class="col-md-6">
                <?= Slider::widget([
                    'id' => $idWidget . '_cbox_resize_width',
                    'name' => $idWidget . '_cbox_resize_width',
                    'sliderColor' => Slider::TYPE_GREY,
                    'handleColor' => Slider::TYPE_PRIMARY,
                    'pluginOptions' => [
                        'orientation' => 'horizontal',
                        'handle' => 'round',
                        'step' => 1,
                        //'tooltip' => 'hide',
                        //'tooltip' => 'always',
                        'step'=>5,
                    ],
                    'pluginEvents' => [
                        'slide' => "function(e) {
                            $('#{$idWidget}').cropbox('resizeThumbBox', {width: e.value});
                        }",
                    ],
                ]); ?>
            </div>
        </div>
        <div class="form-group resizeHeight">
            <label for="<?= $idWidget; ?>_cbox_resize_height" class="col-md-3"><?= CropMe::t('Height'); ?></label>
            <div class="col-md-6">
                <?= Slider::widget([
                    'id' => $idWidget . '_cbox_resize_height',
                    'name' => $idWidget . '_cbox_resize_height',
                    'sliderColor' => Slider::TYPE_GREY,
                    'handleColor' => Slider::TYPE_PRIMARY,
                    'pluginOptions' => [
                        'orientation' => 'horizontal',
                        'handle' => 'round',
                        'step' => 1,
                        //'tooltip' => 'hide',
                        //'tooltip' => 'always',
                        'step'=>5,
                    ],
                    'pluginEvents' => [
                        'slide' => "function(e) {
                            $('#{$idWidget}').cropbox('resizeThumbBox', {height: e.value});
                        }",
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?php
echo Html::activeHiddenInput($cropModel, $attributeCropInfo);
