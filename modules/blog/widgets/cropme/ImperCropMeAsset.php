<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 21.04.2015
 * Time: 21:55
 */
namespace modules\blog\widgets\cropme;
use yii\web\AssetBundle;

class ImperCropMeAsset extends AssetBundle {
    public $sourcePath = '@modules/blog/widgets/cropme/assets';
    public $css = [
    ];
    public $js = [
        //'js/cropbox.js',
        'js/impercropme.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'modules\blog\widgets\cropme\CropMeAsset'
    ];
}