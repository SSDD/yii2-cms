/**
 * Created by igor on 21.04.2015.
 */
if (!RedactorPlugins) var RedactorPlugins = {};

RedactorPlugins.impercropme = function() {
    return {
        init: function() {
            if (!this.opts.imageManagerJson) {
                return;
            }

            var button = this.button.add('image', 'Image Manager');
            this.button.addCallback(button, this.impercropme.show);

            this.impercropme.setImagesContext();
            $('.re-html').bind('click', this.impercropme.setImagesContext);
        },
        setImagesContext: function(){
            this.$editor.find('img').each($.proxy(function(i, img) {
                var $img = $(img);

                // IE fix (when we clicked on an image and then press backspace IE does goes to image's url)
                $img.closest('a').on('click', function(e) { e.preventDefault(); });

                if (this.utils.browser('msie'))
                    $img.attr('unselectable', 'on');

                this.impercropme.imageContext($img);
            }, this));
        },
        show: function() {
            this.impercropme.setImagesContext();

            this.modal.load('image', 'Crop Me Manager', 850);
            this.modal.show();

            var $modal = this.modal.getModal();

            this.modal.createTabber($modal);
            if (this.opts.uploadAndCrop) {
                this.modal.addTab(1, 'Upload', 'active');
            }
            this.modal.addTab(2, 'Choose');

            if (this.opts.uploadAndCrop) {
                $('#redactor-modal-image-droparea').addClass('redactor-tab redactor-tab1');

                var imperCropUrl = $('#imperCropMePlugin').attr('value');

                this.progress.show();
                var _this = this;
                $('#redactor-modal-image-droparea').load(imperCropUrl, function () {
                    _this.progress.hide();
                });
            }

            var $box = $('<div id="redactor-image-manager-box" style="overflow-x: auto; overflow-y: hidden;" class="redactor-tab redactor-tab2">').hide();
            $modal.append($box);

            $('a[rel="tab2"]').on('click', this.impercropme.scan);
            if (!this.opts.uploadAndCrop) {
                $('a[rel="tab2"]').click();
            }
        },
        scan: function() {
            $('#redactor-image-manager-box').html('Scan in progress...');
            this.progress.hide(); // if it was not hidden before in the Upload tab
            this.progress.show();

            $.ajax({
                dataType: "json",
                cache: false,
                url: this.opts.imageManagerJson,
                headers: {'Cache-Control' : 'no-cache, max-age=0, proxy-revalidate', 'Last-Modified': '-1', 'Pragma': 'no-cache'},
                success: $.proxy(function(data)
                {
                    $('#redactor-image-manager-box').html('');

                    $.each(data, $.proxy(function(key, val)
                    {
                        // title
                        var thumbtitle = '';
                        if (typeof val.title !== 'undefined') {
                            thumbtitle = val.title;
                        }

                        val.thumb += '?rnd=' + Math.random();

                        var img =
                            '<div style=" padding:5px;float:left;">'+
                                '<img src="' + val.thumb + '" rel="' + val.image + '" title="' + thumbtitle + '" style="width: 160px; height: 120px; cursor: pointer; padding: 5px;border: 1px solid #aeaeae; border-radius: 3px;" />'+
                                '<div style="width: 160px; overflow-y: hidden; margin-top: 5px;">';

                        if (this.opts.imageRemoveJson) {
                            img +=  '<button class="btn btn-default btn-xs img-remove"><span class="glyphicon glyphicon-trash"></span></button>';
                        }
                        img +=      '<span style="font-size:10px;">&nbsp;' + thumbtitle + '</span>' +
                                '</div>'+
                            '</div>';
                        img = $(img);

                        $('#redactor-image-manager-box').append(img);
                        $(img).find('img').click($.proxy(this.impercropme.insert, this));
                        $(img).find('.img-remove').click($.proxy(this.impercropme.remove, this));

                    }, this));

                    $('#redactor-progress').fadeOut(1500, function() {
                        $(this).remove();
                    });

                }, this)

            });

        } ,
        imageContext: function($image) {
            var _this = this;
            var _image = $image;

            var imageMenu = [
                {'Edit Image':
                    function(menuItem, menu) {
                        _this.impercropme.editImage($(this), _image);
                    }
                }
            ];

            $image.contextMenu(imageMenu);

        },
        editImage: function(image, _image) {

            this.modal.load('image', 'Edit Image', 705);
            this.modal.show();
            $('#redactor-modal-body').html(
                '<section id="redactor-modal-image-edit">'
                // Title
                + '<label>Title</label>'
                + '<input type="text" id="redactor-image-title" />'

                // Alt
                + '<label>Alt</label>'
                + '<textarea id="redactor-image-alt"  style="height:60px;"></textarea>'

                // Text
                + '<label>Text</label>'
                + '<textarea id="redactor-image-text"  style="height:60px;"></textarea>'

                // Pop
                + '<label class="redactor-image-link-option"><input type="checkbox" id="redactor-image-pop"> Pop Class</label>'

                // Position
                + '<label class="redactor-image-position-option">Position Class</label>'
                + '<select class="redactor-image-position-option" id="redactor-image-align">'
                + '<option value="none">' + this.lang.get('none') + '</option>'
                + '<option value="left">' + this.lang.get('left') + '</option>'
                + '<option value="center">' + this.lang.get('center') + '</option>'
                + '<option value="right">' + this.lang.get('right') + '</option>'
                + '</select>'
                + '</section>');

            // Add buttons
            this.modal.createCancelButton();
            this.image.buttonDelete = this.modal.createDeleteButton(this.lang.get('_delete'));
            this.image.buttonSave = this.modal.createActionButton(this.lang.get('save'));
            this.modal.setButtonsWidth();

            // Init form inputs
            $('#redactor-image-title').val(_image.attr('title'));
            $('#redactor-image-alt').html(_image.attr('alt'));
            $('#redactor-image-text').html(_image.next('.post-img-text').find('p').html());

            if (_image.hasClass('img-pop')) {
                $('#redactor-image-pop').prop('checked', true);
            }

            var floatValue = (_image.parent().hasClass('post-img-left') ? 'left' :
                _image.parent().hasClass('post-img-right') ? 'right' :
                    _image.parent().hasClass('post-img-center') ? 'center' : 'none');
            $('#redactor-image-align').val(floatValue);

            // On delete
            this.image.buttonDelete.on('click', $.proxy(function() {
                this.image.remove(image);
            }, this));

            // On save
            this.image.buttonSave.on('click', $.proxy(function() {
                this.image.hideResize();
                this.buffer.set();

                if (!$(_image).parent().hasClass('post-img')) {
                    $(_image).wrap('<div class="post-img img-responsive"></div>');
                }

                // Set title
                _image.attr('title', $('#redactor-image-title').val());

                // Set alt
                _image.attr('alt', $('#redactor-image-alt').val());

                // Set text
                _image.next('.post-img-text').remove();
                var img_text = $.trim($('#redactor-image-text').val());
                if (img_text) {
                    $(_image).after('<div class="post-img-text" style="width:' + $(_image).width() + 'px;"><p>'+img_text+'</p></div>');
                }

                // Set pop class
                _image.removeClass('img-pop');
                if ($('#redactor-image-pop').prop('checked')) {
                    _image.addClass('img-pop');
                }

                this.impercropme.setImagePosition(_image);

                this.modal.close();
                this.observe.images();
                this.code.sync();

            }, this));
        },
        setImagePosition: function($image) {
            var floating = $('#redactor-image-align').val();

            var imageFloat = '';
            var imageDisplay = '';
            var imageMargin = '';

            var imageLeftClass = 'post-img-left',
                imageRightClass = 'post-img-right',
                imageCenterClass = 'post-img-center',
                imageClass = 'post-img-' + floating;

            $image.parent()
                .removeClass(imageLeftClass)
                .removeClass(imageRightClass)
                .removeClass(imageCenterClass);

            if (floating != 'none') {
                $image.parent().addClass(imageClass);
            }
        },
        insert: function(e) {
            this.image.insert('<img src="' + $(e.target).attr('rel') + '" class="img-responsive" />');
            this.impercropme.setImagesContext();
        },
        remove: function(e) {
            var imgName = $(e.target).parent().prev().attr('title');

            if (confirm('Remove ' + imgName + '?')) {
                // perform ajax query
                var data = new FormData();
                data.append('imageName', imgName);

                $.ajax({
                    url: this.opts.imageRemoveJson+'?imageName='+imgName,
                    dataType: "json",
                    cache: false,
                    success: function(response) {
                        $('#message').html('done!');
                        $(e.target).parent().parent().remove();
                    },
                    error: function(response) {
                        $('#message').html('error!');
                    }
                });
            }
        }
    };
};
