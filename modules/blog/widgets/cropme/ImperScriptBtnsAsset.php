<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 23.04.2015
 * Time: 10:08
 */
namespace modules\blog\widgets\cropme;
use yii\web\AssetBundle;

class ImperScriptBtnsAsset extends AssetBundle {
    public $sourcePath = '@modules/blog/widgets/cropme/assets';
    public $css = [
    ];
    public $js = [
        'js/scriptbuttons.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}