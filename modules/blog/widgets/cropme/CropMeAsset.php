<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 21.04.2015
 * Time: 15:57
 */
namespace modules\blog\widgets\cropme;
use yii\web\AssetBundle;

class CropMeAsset extends AssetBundle {
    public $sourcePath = '@modules/blog/widgets/cropme/assets';
    public $css = [
        'css/cropbox.css',
        //'css/codemirror.css',
        'css/jquery.contextmenu.css',
    ];
    public $js = [
        'js/cropbox.js',
        'js/cropme.js',
        //'js/codemirror.min.js'
        'js/jquery.contextmenu.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'kartik\slider\SliderAsset',
        'kartik\icons\FontAwesomeAsset',
    ];
}