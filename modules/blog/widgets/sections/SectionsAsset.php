<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 01.06.2015
 * Time: 16:26
 */
namespace modules\blog\widgets\sections;
use yii\web\AssetBundle;

class SectionsAsset extends AssetBundle {
//    public $sourcePath = '@modules/blog/widgets/sections/assets';
    public $sourcePath = '@root/modules/blog/widgets/sections/assets';
		
    public $css = [
        'css/sections.css',
    ];
    public $js = [
        //'js/sections.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}