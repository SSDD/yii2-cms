<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 25.05.2015
 * Time: 15:47
 */
namespace modules\blog\widgets\sections;

use Yii;
use yii\base\InvalidConfigException;
use modules\blog\components\Widget;
use modules\blog\models\Category;
use modules\blog\models\Post;

class Sections extends Widget {

	public $name = 'sections';

    public $newPostsTitle = 'New Posts';
    /**
     * @var int
     */
    public $newPostsCount = 5;

    /**
     * @var array
     */
    public $exceptPosts = [];

    /**
     * @var array
     */
    public $onlyCategories = [];
    /**
     * @var
     */
    public $exceptCategories = [];

    /**
     * @var array
     */
    public $altCategoryNames = [];

    /**
     * More Posts is an anchor for the more link and is shown under the category block. It substitutes the link
     * to the category from the header of the block. Defaults to false.
     * @var string|bool
     */
    public $morePosts = false;

    /**
     * Categories in sections:
     * - category Id is the key;
     * - section is the value.
     * @var array
     */
    protected $catInSec = [];

    protected $categoriesArray = [];

    protected $newPostsIds = [];
    protected $postsArray = [];

	// public function getViewPath() {
		// return '@root/modules/blog/widgets/sections/views';
	// }
	
    public function init() {
        parent::init();
		
        SectionsAsset::register($this->view);

        if (!is_array($this->exceptPosts)) {
            throw new InvalidConfigException('The "exeptPosts" property must be set as array [post_id1, post_id2,...].');
        }

        if (!is_array($this->exceptCategories)) {
            throw new InvalidConfigException('The "exeptCategories" property must be set as array [cat_id1, cat_id2,...].');
        }

        if (!is_array($this->altCategoryNames)) {
            throw new InvalidConfigException('The "altCategoryNames" property must be set as array [cat_id1=>cat_name1, cat_id2=>cat_name2,...].');
        }

        $this->categoriesArray = Category::getCategories();

        if (count($this->onlyCategories)) {
            $this->onlyCategories = array_flip($this->onlyCategories);

            foreach($this->onlyCategories as $id => $cat) {
                $this->onlyCategories[$id] = $this->categoriesArray[$id];
                $this->onlyCategories[$id]['parent_id'] = 0;
            }

            $this->categoriesArray = $this->onlyCategories;
        }

        $this->setRootSections();
    }

    public function run() {
        $this->postsArray = $this->fetchPosts();
        $this->groupPostsBySections();
        $pairSections = $this->pairSections();


        return $this->render('sections', [
            'newPostsTitle' => $this->newPostsTitle,
            'newPosts' => $this->postsArray['new'],
            'allPosts' => $this->postsArray['rest'],
            'categories' => $this->categoriesArray,
            'pairSections' => $pairSections,
            'morePosts' => $this->morePosts,
        ]);
    }

    protected function setRootSections() {
        $root_id = 0;

        foreach ($this->categoriesArray as $id => $cat) {
            if (isset($this->altCategoryNames[$id])) {
                $this->categoriesArray[$id]['name'] = $this->altCategoryNames[$id];
            }

            if ($cat['parent_id']) {
                if ($cat['parent_id'] !== $root_id) {
                    $root_id = $this->findRootSection($cat['id']);
                }
            }
            else {
                $root_id = $cat['id'];
            }

            $this->catInSec[$cat['id']] = $root_id;
        }
    }

    /**
     * Searcher for the root section id of a certain category.
     * @param $categoriesArray
     * @param $parent_id
     * @return mixed
     */
    protected function findRootSection($category_id) {
        while ($parent_id = $this->categoriesArray[$category_id]['parent_id']) {
            $category_id = $parent_id;
        }
        return $category_id;
    }

    protected function getRoot($category_id) {
        if (!isset($this->catInSec[$category_id])) {
            throw new InvalidConfigException('The "root" could not de defined for the "category_id"='.$category_id);
        }
        return $this->catInSec[$category_id];
    }

    protected function pairSections() {

        $postsArray = $this->postsArray['rest'];

        $sections = [];
        $pair = [];

        foreach ($this->categoriesArray as $cat) {

            // going through root sections only
            if ($cat['parent_id'] == 0 && isset($postsArray[$cat['id']])) {
                if (isset($postsArray[$cat['id']])) {
                    $cat['postCount'] = count($postsArray[$cat['id']]);
                }

                if (!$cat['postCount']) {
                    continue;
                }

                $cat['posts'] = $postsArray[$cat['id']];

                if (count($pair) < 2) {
                    $pair[] = $cat;
                }
                else {
                    $sections[] = $pair;
                    $pair = [$cat];
                }
            }
        }

        if (count($pair)) {
            $sections[] = $pair;
        }

        return $sections;
    }

    protected function fetchPosts() {
        $postsArray = [];
        $union = null;

        foreach ($this->categoriesArray as $cat_id => $category) {

            if (!$category['postCount']) {
                continue;
            }

            $query = (new \yii\db\Query())
                ->select(
                    [
                        'id',
                        'date',
                        'category_id',
                        'category_url_id',
                        'alias',
                        'name',
                        'anons',
                        'images',
                    ])
                ->from(Post::tableName())
                ->andWhere(['status' => Post::POST_PUBLISHED])
                ->andWhere(['category_id' => $cat_id])
                ->orderBy(['date' => SORT_DESC])
                ->limit(5);

            if (!$union) {
                $union = $query;
            }
            else {
                $union->union($query);
            }

            //->createCommand()
            //echo $query->sql.'<hr>';
        }
        if ($union) {
            $postsArray = $union->all();
        }

        return $postsArray;
    }

    protected function groupPostsBySections() {
        $resultArray = [
            'new' => [],
            'rest' => []
        ];

        $newPosts = [];
        $root_id = 0;

        $posts = [];

        foreach($this->postsArray as $i => $post) {

            if (in_array($post['id'], $this->exceptPosts) ||
                in_array($post['category_id'], $this->exceptCategories)) {

                continue;
            }


            if ($this->getRoot($post['category_id']) !== $root_id) {

                if ($root_id && count($posts)) {
                    rsort($posts);
                    $newPosts[$posts[0]['date']] = $posts[0];
                    $resultArray['rest'][$root_id] = $posts;
                    $posts = [];
                }

                $root_id = $this->getRoot($post['category_id']);
            }

            $posts[$post['date']] = $post;
        }

        if (count($posts)) {
            rsort($posts);
            $newPosts[$posts[0]['date']] = $posts[0];
            $resultArray['rest'][$root_id] = $posts;
        }

        rsort($newPosts);
        $newPosts = array_slice($newPosts, 0, $this->newPostsCount);

        foreach($newPosts as $post) {
            $this->newPostsIds[$post['id']] = $post['id'];
        }

        $resultArray['new'] = $newPosts;

        $this->postsArray = $resultArray;
    }

    public function isNewPost($post_id) {
        return isset($this->newPostsIds[$post_id]);
    }
}
