<?php
use yii\helpers\Html;
use modules\blog\components\ContentHelper;

$postUrl = ContentHelper::getPostUrl($post['category_url_id'], $post['id'], $post['alias']);

?>
<div class="section-post">

    <?= Html::a(
        Html::img(ContentHelper::getImageSrc('mini', $post['images']), [
            'class' => 'img-thumbnail'
        ]),
        $postUrl
    );
    ?>

    <?= Html::a(
        $post['name'],
        //['post/index', 'alias' => $post['alias']]
        $postUrl
    );
    ?>

    <?php if ($showCategory) { ?>
    <p class="section-post-text">
        <small>
            <span class="glyphicon glyphicon-paperclip text-primary"></span>&nbsp;
            <?= Html::a(
                $category['name'],
                ['category/index', 'alias' => $category['alias']]
            );
            ?>
            <br>
            <?//= $post['date']; ?>
        </small>
    </p>
    <?php } ?>
</div>