<?php
use yii\helpers\Html;
?>
<div class="posts-block">

    <h3 class="posts-block-title text-center">
        <span class="glyphicon glyphicon-folder-open section-icon"></span>&nbsp;
        <?php
        if (!$morePosts) {
            echo Html::a($section['name'],['category/index', 'alias' => $section['alias']]);
        }
        else {
            echo $section['name'];
        }
        ?>
    </h3>

    <?php
        foreach($section['posts'] as $index => $post) {
            if ($this->context->isNewPost($post['id'])) {
                continue;
            }

            echo $this->render('_post', [
                'post' => $post,
                'showCategory' => $section['id'] !== $categories[$post['category_id']]['id'],
                'category' => $categories[$post['category_id']],
            ]);
        }
    ?>
    <?php if ($morePosts): ?>
        <div class="category-more-link text-center">
            <?=Html::a($morePosts,['category/index', 'alias' => $section['alias']]); ?>
        </div>
    <?php endif; ?>
</div>