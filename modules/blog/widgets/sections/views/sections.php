<?php if (count($newPosts)) { ?>
<div class="posts-block">
    <?= $this->render('new_posts', [
        'newPostsTitle' => $newPostsTitle,
        'posts' => $newPosts,
        'categories' => $categories
    ]); ?>
</div>
<?php } ?>

<?php foreach($pairSections as $pair) { ?>
    <?php if (count($pair) == 2): ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $this->render('section', [
                    'section' => $pair[0],
                    'categories' => $categories,
                    'morePosts' => $morePosts,
                ]);
            ?>
        </div>

        <div class="col-sm-6">
            <?= $this->render('section', [
                    'section' => $pair[1],
                    'categories' => $categories,
                    'morePosts' => $morePosts,
                ]);
            ?>
        </div>
    </div>
    <?php else: ?>
        <div class="col-sm-offset-3 col-sm-6">
            <?= $this->render('section', [
                    'section' => $pair[0],
                    'categories' => $categories,
                    'morePosts' => $morePosts,
                ]);
            ?>
        </div>
    <?php endif; ?>
<?php } ?>