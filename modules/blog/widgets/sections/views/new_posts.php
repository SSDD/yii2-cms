<h3 class="posts-block-title"><?= $newPostsTitle; ?></h3>

<div class="row">
    <div class="col-sm-6">
        <?= $this->render('_fresh', [
            'post' => $posts[0],
            'showCategory' => true,
            'category' => $categories[$posts[0]['category_id']]
        ]); ?>
    </div>

    <div class="col-sm-6">
        <?php
            for($i = 1; $i < count($posts); $i++) {
                echo $this->render('_post', [
                    'post' => $posts[$i],
                    'showCategory' => true,
                    'category' => $categories[$posts[$i]['category_id']]
                ]);
            }
        ?>
    </div>
</div>
