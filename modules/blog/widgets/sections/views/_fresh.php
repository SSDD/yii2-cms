<?php
use yii\helpers\Html;
use yii\helpers\Url;
use modules\blog\components\ContentHelper;

$postUrl = $postUrl = ContentHelper::getPostUrl($post['category_url_id'], $post['id'], $post['alias']);
//Url::to(['post/index', 'alias' => $post['alias']]);
?>
<div class="fresh-post">
    <div class="fresh-img post-img img-responsive post-img-left">
        <?= Html::a(
            Html::img(ContentHelper::getImageSrc('', $post['images']), ['class' => 'img-responsive']),
            $postUrl
        ); ?>

        <p class="fresh-img">
            <?= Html::a($post['name'], $postUrl); ?>

            <?php if ($showCategory) { ?>
            <br>
            <small>
                <span class="glyphicon glyphicon-paperclip" style="color:#337ab7;"></span>&nbsp;
                <?= Html::a(
                        $category['name'],
                        ['category/index', 'alias' => $category['alias']],
                        ['style' =>'font-size:95%;']
                    );
                ?>
            </small>
            <?php } ?>
        </p>

        <div class="fresh-img-text">
            <p>
                <?= ContentHelper::anons($post['anons'], 0, 120); ?>
            </p>
        </div>
    </div>
</div>