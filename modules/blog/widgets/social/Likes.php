<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 12.06.2015
 * Time: 9:33
 */
namespace modules\blog\widgets\social;

use Yii;
use modules\blog\components\Widget;

class Likes extends Widget {

	public $name = 'social';
	
    public $twitterAccount = '';

    public function init() {
        parent::init();
        LikesAsset::register($this->view);
    }

    public function run() {
        return $this->render('index', ['twitterAccount' => $this->twitterAccount]);
    }

}