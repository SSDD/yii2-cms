<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 12.06.2015
 * Time: 9:35
 */
namespace modules\blog\widgets\social;
use yii\web\AssetBundle;

class LikesAsset extends AssetBundle {
    public $sourcePath = '@root/modules/blog/widgets/social/assets';
    public $css = [
        'css/social-likes_birman.css',
    ];
    public $js = [
        'js/social-likes.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}