<p>Если Вам понравилась статья, Вы можете добавить её в свои закладки:</p>

<div class="social-likes">
    <div class="facebook" title="Поделиться ссылкой на Фейсбуке">Facebook</div>
    <div class="twitter" data-via="<?= $twitterAccount; ?>" title="Поделиться ссылкой в Твиттере">Twitter</div>
    <div class="vkontakte" title="Поделиться ссылкой во Вконтакте">Вконтакте</div>
    <div class="mailru" title="Поделиться ссылкой в Моём мире">Мой мир</div>
    <div class="odnoklassniki" title="Поделиться ссылкой в Одноклассниках">Одноклассники</div>
    <div class="plusone" title="Поделиться ссылкой в Гугл-плюсе">Google+</div>
</div>