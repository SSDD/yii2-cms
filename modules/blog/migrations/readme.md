How to create (an empty) migration file:
========================================

Install all migrations at once:

**Linux**


```
./yii migrate --migrationPath=@modules/blog/migrations
```

**Windows XAMPP**

```
yii migrate --migrationPath=@modules/blog/migrations
```