<?php

use yii\db\Schema;
use yii\db\Migration;

class m150612_113701_create_comment_tbl extends Migration
{
    public function up()
    {
        // MySql table options
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        // Comments table
        $this->createTable('{{%comment}}', [
            'id' => Schema::TYPE_PK,
            'post_id' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'comment_id' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'user_name' => Schema::TYPE_STRING . ' NOT NULL',
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'comment' => Schema::TYPE_TEXT . ' NOT NULL',
            'date' => Schema::TYPE_DATETIME . ' NOT NULL DEFAULT "0000-00-00 00:00:00"',
            'status' => 'ENUM("pending", "approved", "banned") NOT NULL DEFAULT "pending"',
            'ip' => Schema::TYPE_STRING . ' NOT NULL DEFAULT "000.000.000.000"',
        ], $tableOptions);

        // Indexes
        $this->createIndex('post_id', '{{%comment}}', ['post_id', 'comment_id'], true);
        $this->createIndex('ip', '{{%comment}}', ['ip', 'post_id'], false);
        $this->createIndex('post_id_2', '{{%comment}}', ['post_id', 'status'], false);

        // Foreign Keys
        $this->addForeignKey('post', '{{%comment}}', 'post_id', '{{%post}}', 'id');

        // Fulltext only for MyIsam table type
        //$this->execute('ALTER TABLE {{%post}} ADD FULLTEXT full_text (title, text)');
    }

    public function down()
    {
        $this->dropTable('{{%comment}}');
    }
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
