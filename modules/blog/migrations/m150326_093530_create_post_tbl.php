<?php

/**
 * How to create (an empty) migration file:
 * D:\xampp\htdocs\is.cms>php yii migrate/create create_post_tbl --migrationPath=@modules/blog/migrations
 *
 * How run (the blog module's) migrations:
 * D:\xampp\htdocs\is.cms>php yii migrate --migrationPath=@modules/blog/migrations
 *
 */

use yii\db\Schema;
use yii\db\Migration;

class m150326_093530_create_post_tbl extends Migration
{
    public function up()
    {
        // MySql table options
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        // Posts table
        $this->createTable('{{%post}}', [
            'id' => Schema::TYPE_PK,
            'category_id' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'category_url_id' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'date' => Schema::TYPE_DATETIME . ' NOT NULL DEFAULT "0000-00-00 00:00:00"',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'keywords' => Schema::TYPE_STRING . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'alias' => Schema::TYPE_STRING . ' NOT NULL',
            'description' => Schema::TYPE_TEXT . ' NOT NULL',
            'anons' => Schema::TYPE_TEXT . ' NOT NULL',
            'text' => Schema::TYPE_TEXT . ' NOT NULL',
            'images' => Schema::TYPE_TEXT . ' NOT NULL',
            'tags' => Schema::TYPE_TEXT . ' NOT NULL',
            'status' => 'ENUM("published", "draft") NOT NULL DEFAULT "draft"',
            'visits' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'comments_state' => 'ENUM("on", "off", "hidden") NOT NULL DEFAULT "on"'
        ], $tableOptions);

        // Indexes
        $this->createIndex('name', '{{%post}}', 'name', true);
        $this->createIndex('alias', '{{%post}}', 'alias', true);
        $this->createIndex('category_id', '{{%post}}', ['category_id', 'status'], false);
        $this->createIndex('category_url_id', '{{%post}}', ['category_url_id'], false);
        $this->createIndex('date', '{{%post}}', 'date', false);
        $this->createIndex('user_id', '{{%post}}', 'user_id', false);
        $this->createIndex('visits', '{{%post}}', 'visits', false);
        $this->createIndex('status', '{{%post}}', 'status', false);
        // Foreign Keys
        $this->addForeignKey('category', '{{%post}}', 'category_id', '{{%category}}', 'id');
        $this->addForeignKey('category_2', '{{%post}}', 'category_url_id', '{{%category}}', 'id');
        $this->addForeignKey('user', '{{%post}}', 'user_id', '{{%user}}', 'id');

        // Fulltext only for MyIsam table type
        //$this->execute('ALTER TABLE {{%post}} ADD FULLTEXT full_text (title, text)');
    }

    public function down()
    {
        $this->dropTable('{{%post}}');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
