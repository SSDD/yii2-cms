<?php

use yii\db\Schema;
use yii\db\Migration;

class m150319_123309_create_category_tbl extends Migration
{
    public function up()
    {
        // MySql table options
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        // Categories table
        $this->createTable('{{%category}}', [
            'id' => Schema::TYPE_PK,
            'order' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'parent_id' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'title' => Schema::TYPE_STRING,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'alias' => Schema::TYPE_STRING . ' NOT NULL',
            'crumb' => Schema::TYPE_STRING . ' NOT NULL',
            'snippet' => Schema::TYPE_TEXT . ' NOT NULL',
            'text' => Schema::TYPE_TEXT . ' NOT NULL',
            'image' => Schema::TYPE_STRING . ' NOT NULL'
        ], $tableOptions);

        // Indexes
        $this->createIndex('order', '{{%category}}', 'order', false);
        $this->createIndex('name', '{{%category}}', 'name', true);
        $this->createIndex('alias', '{{%category}}', 'alias', true);
        $this->createIndex('parent_name', '{{%category}}', ['parent_id', 'name'], true);
        $this->createIndex('parent_alias', '{{%category}}', ['parent_id', 'alias'], true);
    }

    public function down()
    {
        $this->dropTable('{{%category}}');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
