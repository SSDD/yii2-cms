<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 26.06.2015
 * Time: 14:39
 */

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\rbac\AuthorRule;
use common\models\User;

class RbacController extends Controller {
    public function actionInit() {

        $auth = Yii::$app->authManager;

        // Permissions
        // ===========

        // Backend

        // add "cabinetItems" permission
        $cabinetItems = $auth->createPermission('cabinetItems');
        $cabinetItems->description = 'Access cabinet items';
        $auth->add($cabinetItems);

        // Categories Permissions

        // add "createCategory" permission
        $createCategory = $auth->createPermission('createCategory');
        $createCategory->description = 'Create a category';
        $auth->add($createCategory);

        // add "updateCategory" permission
        $updateCategory = $auth->createPermission('updateCategory');
        $updateCategory->description = 'Update category';
        $auth->add($updateCategory);

        // add "deleteCategory" permission
        $deleteCategory = $auth->createPermission('deleteCategory');
        $deleteCategory->description = 'Delete category';
        $auth->add($deleteCategory);

        // add "accessCategories" permission
        $accessCategories = $auth->createPermission('accessCategories');
        $accessCategories->description = 'Access categories';
        $auth->add($accessCategories);

        // Posts Permissions

        // add "createPost" permission
        $createPost = $auth->createPermission('createPost');
        $createPost->description = 'Create a post';
        $auth->add($createPost);

        // add "updatePost" permission
        $updatePost = $auth->createPermission('updatePost');
        $updatePost->description = 'Update post';
        $auth->add($updatePost);

        // add "deletePost" permission
        $deletePost = $auth->createPermission('deletePost');
        $deletePost->description = 'Delete post';
        $auth->add($deletePost);

        // add "accessPosts" permission
        $accessPosts = $auth->createPermission('accessPosts');
        $accessPosts->description = 'Access posts';
        $auth->add($accessPosts);

        // Comments Permissions

        // add "createComment" permission
        $createComment = $auth->createPermission('createComment');
        $createComment->description = 'Create a comment';
        $auth->add($createComment);

        // add "updateComment" permission
        $updateComment = $auth->createPermission('updateComment');
        $updateComment->description = 'Update comment';
        $auth->add($updateComment);

        // add "deleteComment" permission
        $deleteComment = $auth->createPermission('deleteComment');
        $deleteComment->description = 'Delete comment';
        $auth->add($deleteComment);

        // add "accessComments" permission
        $accessComments = $auth->createPermission('accessComments');
        $accessComments->description = 'Access comments';
        $auth->add($accessComments);

        // Profile Permissions

        // add "updateProfile" permission
        $updateProfile = $auth->createPermission('updateProfile');
        $updateProfile->description = 'Update profile';
        $auth->add($updateProfile);

        // Tools Permissions

        // add "clearCache" permission
        $clearCache = $auth->createPermission('clearCache');
        $clearCache->description = 'Clear cache';
        $auth->add($clearCache);

        // add "sphinxReindex" permission
        $sphinxReindex = $auth->createPermission('sphinxReindex');
        $sphinxReindex->description = 'Sphinx RT reindex';
        $auth->add($sphinxReindex);

        // Rules
        // =====

        // add the AuthorRule
        $rule = new AuthorRule;
        $auth->add($rule);

        // add the "updateOwnPost" permission and associate the rule with it.
        $updateOwnPost = $auth->createPermission('updateOwnPost');
        $updateOwnPost->description = 'Update own post';
        $updateOwnPost->ruleName = $rule->name;
        $auth->add($updateOwnPost);

        // Roles
        // =====

        // User

        // add "user" role and give this role the "createPost" permission
        $user = $auth->createRole('user');
        $auth->add($user);
        $auth->addChild($user, $updateProfile);
        // todo

        // Author

        // add "author" role and give this role the "createPost" permission
        $author = $auth->createRole('author');
        $auth->add($author);
        // cabinet access
        $auth->addChild($author, $cabinetItems);
        // posts
        $auth->addChild($author, $accessPosts);
        $auth->addChild($author, $createPost);
        // "updateOwnPost" will be used from "updatePost"
        $auth->addChild($updateOwnPost, $updatePost);
        // allow "author" to update their own posts
        $auth->addChild($author, $updateOwnPost);
        // children
        $auth->addChild($author, $user);

        // Admin

        // add "admin" role and give this role the "updatePost" permission
        // as well as the permissions of the "author" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        // categories
        $auth->addChild($admin, $accessCategories);
        $auth->addChild($admin, $createCategory);
        $auth->addChild($admin, $updateCategory);
        $auth->addChild($admin, $deleteCategory);
        // posts
        $auth->addChild($admin, $deletePost);
        $auth->addChild($admin, $updatePost);
        // comments
        $auth->addChild($admin, $accessComments);
        $auth->addChild($admin, $createComment);
        $auth->addChild($admin, $updateComment);
        $auth->addChild($admin, $deleteComment);
        // tools
        $auth->addChild($admin, $clearCache);
        $auth->addChild($admin, $sphinxReindex);
        // children
        $auth->addChild($admin, $author);

        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        //$auth->assign($admin, 1);
        //$auth->assign($author, 2);
        //$auth->assign($user, 3);

        return 0;
    }

    public function actionAssign($userId, $roleName)
    {
        $exitCode = 0;

        $userId = (int)$userId;

        $user = User::findIdentity($userId);

        $msg = '';

        if ($user) {
            $auth = Yii::$app->authManager;

            do {
                $roles = $auth->getRoles();
                if (!isset($roles[$roleName])) {
                    $msg = 'Unknown roleName "'.$roleName.'"'."\n";
                    break;
                }

                $roles = $auth->getRolesByUser($userId);

                if (isset($roles[$roleName])) {
                    $msg = 'Person: '.$user->username.' '.$user->fullname.' already has a role "'.$roleName.'"'."\n";
                    break;
                }

                $msg = 'Person: '.$user->username.' '.$user->fullname."\n";
                $author = $auth->getRole($roleName);
                $auth->assign($author, $userId);

                $msg .= 'Congrats! '.$user->fullname.' has got the role "'.$roleName.'" just now.'."\n";

            } while(false);

        }
        else {
            $msg = 'Unknown person with id='.$userId."\n";
            $exitCode = 1;
        }

        echo $msg;

        return $exitCode;
    }

    public function actionRevoke($userId, $roleName)
    {
        $exitCode = 0;

        $userId = (int)$userId;

        $user = User::findIdentity($userId);

        $msg = '';

        if ($user) {
            $auth = Yii::$app->authManager;

            do {
                $roles = $auth->getRoles();
                if (!isset($roles[$roleName])) {
                    $msg = 'Unknown roleName "'.$roleName.'"'."\n";
                    break;
                }

                $roles = $auth->getRolesByUser($userId);

                if (!isset($roles[$roleName])) {
                    $msg = 'Person: '.$user->username.' '.$user->fullname.' does not have a role "'.$roleName.'"'."\n";
                    break;
                }

                $msg = 'Person: '.$user->username.' '.$user->fullname."\n";
                $author = $auth->getRole($roleName);
                $auth->revoke($author, $userId);

                $msg .= 'Congrats! '.$user->fullname.' got rid of the role "'.$roleName.'" just now.'."\n";

            } while(false);

        }
        else {
            $msg = 'Unknown person with id='.$userId."\n";
            $exitCode = 1;
        }

        echo $msg;

        return $exitCode;
    }

    public function actionRoles($userId)
    {
        $exitCode = 0;

        $userId = (int)$userId;

        $user = User::findIdentity($userId);

        $msg = '';

        if ($user) {
            $auth = Yii::$app->authManager;

            do {
                $roles = $auth->getRolesByUser($userId);

                $userRoles = implode(', ', array_keys($roles));

                $msg = 'Person: '.$user->username.' '.$user->fullname.' roles: "'.$userRoles.'"'."\n";

            } while (false);

        }
        else {
            $msg = 'Unknown person with id='.$userId."\n";
            $exitCode = 1;
        }

        echo $msg;

        return $exitCode;
    }

}