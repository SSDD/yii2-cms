<?php

use yii\helpers\Html;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use themes\backend\ThemeAsset;
use themes\backend\Theme;
use kartik\nav\NavX;

ThemeAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => 'is.CMS',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);

            $menuItems = [
                ['label' => Theme::t('back', 'Home'), 'url' => Yii::$app->urlManagerFrontend->createUrl('/blog/blog/index') ]
            ];

            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => Theme::t('back', 'Login'), 'url' => ['/blog/blog/login']];
            } else {

                $cabinetItems = [];

                if (\Yii::$app->user->can('accessCategories')) {
                    $cabinetItems[] = ['label' => Theme::t('back', 'Categories'), 'url' => ['/blog/category/index']];
                }
                if (\Yii::$app->user->can('accessPosts')) {
                    $cabinetItems[] = ['label' => Theme::t('back', 'Posts'), 'url' => ['/blog/post/index']];
                }
                if (\Yii::$app->user->can('accessComments')) {
                    $cabinetItems[] = ['label' => Theme::t('back', 'Comments'), 'url' => ['/blog/comment/index']];
                }
                if (\Yii::$app->user->can('clearCache')) {
                    $cabinetItems[] = '<li class="divider"></li>';
                    $cabinetItems[] =
                        ['label' => Theme::t('back', 'Clear cache'), 'items' =>
                            [
                                ['label' => Theme::t('back', 'All cache'), 'url' => ['/blog/blog/clear-cache', 'type' => 'all']],
                                '<li class="divider"></li>',
                                ['label' => Theme::t('back', 'Categories'), 'url' => ['/blog/blog/clear-cache', 'type' => 'categories']],
                                ['label' => Theme::t('back', 'Posts & Comments'), 'url' => ['/blog/blog/clear-cache', 'type' => 'posts']],
                                '<li class="divider"></li>',
                                ['label' => Theme::t('back', 'Related posts'), 'url' => ['/blog/blog/clear-cache', 'type' => 'related']],
                                ['label' => Theme::t('back', 'Popular posts'), 'url' => ['/blog/blog/clear-cache', 'type' => 'popular']],
                                ['label' => Theme::t('back', 'Last posts'), 'url' => ['/blog/blog/clear-cache', 'type' => 'last']],
                                ['label' => Theme::t('back', 'Last comments'), 'url' => ['/blog/blog/clear-cache', 'type' => 'comments']],
                            ]
                        ];
                }

                if (\Yii::$app->user->can('sphinxReindex')) {
                    $cabinetItems[] =
                        ['label' => Theme::t('back', 'Sphinx RT-Reindex'), 'url' => ['/blog/blog/reindex']];
                }

                if (count($cabinetItems)) {
                    $menuItems[] = [
                        'label' => Theme::t('back', 'Cabinet'),
                        'url' => ['/blog/blog/index'],
                        'items' => $cabinetItems
                    ];

                }

//                $menuItems[] = [
//                    'label' => Theme::t('back', 'Cabinet'),
//                    'url' => ['/blog/blog/index'],
//                    'items' => [
//                        ['label' => Theme::t('back', 'Categories'), 'url' => ['/blog/category/index']],
//                        ['label' => Theme::t('back', 'Posts'), 'url' => ['/blog/post/index']],
//                        ['label' => Theme::t('back', 'Comments'), 'url' => ['/blog/comment/index']],
//                        '<li class="divider"></li>',
//                        ['label' => Theme::t('back', 'Clear cache'), 'items' =>
//                            [
//                                ['label' => Theme::t('back', 'All cache'), 'url' => ['/blog/blog/clear-cache', 'type' => 'all']],
//                                ['label' => Theme::t('back', 'Categories'), 'url' => ['/blog/blog/clear-cache', 'type' => 'categories']],
//                                ['label' => Theme::t('back', 'Posts & Comments'), 'url' => ['/blog/blog/clear-cache', 'type' => 'posts']],
//                                ['label' => Theme::t('back', 'Related posts'), 'url' => ['/blog/blog/clear-cache', 'type' => 'related']],
//                                ['label' => Theme::t('back', 'Popular posts'), 'url' => ['/blog/blog/clear-cache', 'type' => 'popular']],
//                                ['label' => Theme::t('back', 'Last posts'), 'url' => ['/blog/blog/clear-cache', 'type' => 'last']],
//                                ['label' => Theme::t('back', 'Last comments'), 'url' => ['/blog/blog/clear-cache', 'type' => 'comments']],
//                            ]
//                        ],
//                        ['label' => Theme::t('back', 'Sphinx RT-Reindex'), 'url' => ['/blog/blog/reindex']],
//                    ]
//                ];

                $menuItems[] = [
                    'label' => Theme::t('back', 'Logout') . ' (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/blog/blog/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }

            echo NavX::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
                'activateParents' => true,
                //'encodeLabels' => false
            ]);

            NavBar::end();
        ?>

        <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'homeLink' => isset($this->params['breadcrumbs']) ? ['label' => Theme::t('back', 'Cabinet'), 'url' => ['/blog/blog/index']] : []
        ]) ?>

        <?php
            foreach(Yii::$app->session->getAllFlashes() as $class => $message): ?>
            <br>
            <?= \yii\bootstrap\Alert::widget([
                'options' => [
                    'class' => 'col-sm-10 col-sm-offset-1 text-center alert-'. $class,
                ],
                'body' => $message,
            ]);
            ?>
            <?php
            endforeach;
        ?>
        <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy; is.CMS <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
