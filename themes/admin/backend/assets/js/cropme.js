/**
 * Created by igor on 21.04.2015.
 */

$(function(){
    $('#modalButton').click(function(){
        $('#cropMeModal').modal('show')
            .find('#cropMeBox')
            .load($(this).attr('value'));
    });
});