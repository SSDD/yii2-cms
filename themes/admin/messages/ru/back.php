<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 25.03.2015
 * Time: 18:22
 */
return [
    'Home' => 'Главная',
    'Login' => 'Вход',
    'Logout' =>'Выход',
    'Cabinet' => 'Кабинет',
    'Categories' => 'Категории',
    'Posts' => 'Статьи',
    'Comments' => 'Комментарии',
    // Clear cache menu
    'Clear cache' => 'Очистка кэша',
    'All cache' => 'Весь кэш',
    'Posts & Comments' => 'Статьи и комменты',
    'Related posts' => 'Релевантные статьи',
    'Popular posts' => 'Популярные статьи',
    'Last posts' => 'Новые статьи',
    'Last comments' => 'Новые комментарии',
];