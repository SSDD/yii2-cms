<?php

namespace themes\frontend;
use yii\web\AssetBundle;

class ThemeAsset extends AssetBundle
{
    public $sourcePath = '@themes/frontend/assets';

    public $css = [
        'css/site.css',
    ];
    public $js = [
        'js/scripts.js',
        'js/up.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}
