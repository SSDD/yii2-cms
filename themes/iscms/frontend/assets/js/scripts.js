$(document).ready(function(){

    $('.img-pop').each(function(i) {
        $(this).wrap('<a rel="fancybox" href="'+$(this).attr('src').replace('images', 'images/full')+'" title="'+$(this).attr('title')+'"></a>');
    }) ;

    $('.post-short h3 a').each(function(i){
        $(this).parent().find('post-img').wrap('<a href="'+$(this).attr('href')+'"></a>');
        //alert($(this).attr('href'));
    });

    $('.post-short .post-img').click(function() {
        //alert($(this).parent().find('h3 a').attr('href'));
        document.location = $(this).parent().find('h3 a').attr('href');
    });

    $('.post-img-center').wrap('<div class="post-img-center-wrap"></div>');

    var params = {};
    params[$('meta[name=csrf-param]').prop('content')] = $('meta[name=csrf-token]').prop('content');

    $('#search-bar').load('/search-bar', params);
    $('footer .container').load('/footer', params);
});
