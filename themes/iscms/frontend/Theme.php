<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 19.03.2015
 * Time: 10:18
 */
namespace themes\frontend;
use Yii;

class Theme extends \yii\base\Theme {

    public $pathMap = [
        '@frontend/views' => '@themes/frontend/views',
    ] ;

    public function init() {
        parent::init();
    }

    public static function t($category, $message, $params = [], $language = null) {
        return Yii::t('themes/' . $category, $message, $params, $language);
    }
}