<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 08.05.2015
 * Time: 13:18
 */
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use themes\frontend\Theme;

use kartik\nav\NavX;

NavBar::begin([
    'brandLabel' => '<img src="#" alt="Logo Image" height="60px" width="190px" style="background:#eaeaea;padding: 18px 45px;">',//'is.CMS',
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-default navbar-fixed-top',
        'style' => 'min-height:60px;padding-top:4px;'
    ],
]);

?>

<?php

$menuItems = [
    //['label' => Theme::t('front', 'Home'), 'url' => ['/blog/blog/index']],
    ['label' => Theme::t('front', 'About'), 'url' => ['/blog/blog/about']],
    ['label' => Theme::t('front', 'Contact Us'), 'url' => ['/blog/blog/contact']],
    //['label' => Theme::t('front', 'Site Map'), 'url' => ['/blog/blog/sitemap']],
];
if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => Theme::t('front', 'Signup'), 'url' => ['/blog/blog/signup']];
    $menuItems[] = ['label' => Theme::t('front', 'Login'), 'url' => ['/blog/blog/login']];
} else {
    if (\Yii::$app->user->can('cabinetItems')) {
        $menuItems[] = ['label' => Theme::t('front', 'Cabinet'), 'url' => Yii::$app->urlManagerBackend->createUrl('/blog/blog/index')];
    }

    $menuItems[] = [
        'label' => Theme::t('front', 'Logout') . ' (' . Yii::$app->user->identity->username . ')',
        'url' => ['/blog/blog/logout'],
        'linkOptions' => ['data-method' => 'post']
    ];
}

echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right', 'id' => 'site-menu'],
    'items' => $menuItems,
]);
?>

<div style="float:left;max-width:70%;">

    <span id="search-bar"></span>

    <?= NavX::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => [
            ['label' => 'Статьи', 'items' =>
                Yii::$app->categoryTools->categoryMenuItems()
            ],
//            ['label' => 'Объявления', 'items' => [
//                ['label' => 'об услугах',  'url' => '#', 'options' => ['class' => 'disabled']],
//                ['label' => 'о покупке',  'url' => '#', 'options' => ['class' => 'disabled']],
//                ['label' => 'о продаже',  'url' => '#', 'options' => ['class' => 'disabled']],
//            ]],
    //        ['label' => 'Товары', 'items' => [
    //            ['label' => 'Sub Action',  'items' => [
    //                ['label' => 'Another action', 'url' => '#'],
    //            ]],
    //        ]],
        ],
        'activateParents' => true,
        'encodeLabels' => false
    ]);
    ?>

</div>
<?php
NavBar::end();
?>