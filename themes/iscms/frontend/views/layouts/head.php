<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 08.05.2015
 * Time: 13:14
 */
use themes\frontend\ThemeAsset;
use yii\helpers\Html;
?>

<title><?= Html::encode($this->title) ?></title>
<?= Html::csrfMetaTags() ?>
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->
<?php $this->head();
ThemeAsset::register($this);

$this->registerMetaTag(
    [
        'charset' => Yii::$app->charset
    ]
);

$this->registerMetaTag(
    [
        'name' => 'viewport',
        'content' => 'width=device-width, initial-scale=1.0'
    ]
);
?>