<?php

namespace themes;

use yii\base\BootstrapInterface;

/**
 * Themes bootstrap class.
 */
class Bootstrap implements BootstrapInterface {
    public function bootstrap($app) {
        // Add themes I18N category.
        if (!isset($app->i18n->translations['themes*'])/* && !isset($app->i18n->translations['/*'])*/) {
            $app->i18n->translations['themes*'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@themes/messages',
                //'forceTranslation' => true,
                'fileMap' => [
                    'themes/front' => 'front.php',
                ]
            ];
        }
    }
}
