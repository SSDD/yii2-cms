<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 25.03.2015
 * Time: 18:22
 */
return [
    'Home' => 'Главная',
    'Login' => 'Вход',
    'Logout' =>'Выход',
    'Cabinet' => 'Кабинет',
    'Categories' => 'Категории',
    'Posts' => 'Статьи',
    'Comments' => 'Комментарии',
    'About' => 'О сайте',
    'Contact Us' => 'Контакты',
    'Signup' => 'Регистрация',
];